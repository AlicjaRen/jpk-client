import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../users/shared/models/user.model';
import {AuthenticationService} from '../../security/services/authentication.service';
import {Router} from '@angular/router';
import {UserRole} from '../../users/shared/models/user-role.enum';
import {MatIconRegistry} from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Input() title: string;
  currentUser: User;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    this.matIconRegistry.addSvgIcon(
      'invest',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/invest.svg')
    );
  }

  isAdmin(): boolean {
    return UserRole.ADMIN === UserRole[this.currentUser.userRole];
  }

  onClickTaxpayerList() {
    this.router.navigate(['/user/taxpayer']);
  }

  onClickAdminList() {
    this.router.navigate(['/user/admin']);
  }

  onClickAuditFileList() {
    this.router.navigate(['/audit-file']);
  }

  onClickRecordList() {
    this.router.navigate(['/record']);
  }

  onClickUserProfile() {
    if (UserRole.ADMIN === UserRole[this.currentUser.userRole]) {
      this.router.navigate([`/user/admin/admin-details/${this.currentUser.userId}`]);
    } else {
      this.router.navigate([`/user/taxpayer/taxpayer-details/${this.currentUser.userId}`]);
    }
  }

  redirectHome() {
    if (UserRole.ADMIN === UserRole[this.currentUser.userRole]) {
      this.router.navigate(['/home/admin']);
    } else {
      this.router.navigate(['/home/taxpayer']);
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/user/login']);
  }

  ngOnInit() {
  }

}
