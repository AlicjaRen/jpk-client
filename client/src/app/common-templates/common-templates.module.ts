import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {ConfirmationDialogComponent} from './dialog/confirmation-dialog/confirmation-dialog.component';
import {FooterRelativeComponent} from './footer-relative/footer-relative.component';
import {FooterFixedComponent} from './footer-fixed/footer-fixed.component';

@NgModule({
  declarations: [
    ToolbarComponent,
    ConfirmationDialogComponent,
    FooterRelativeComponent,
    FooterFixedComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule
  ],
  exports: [
    ToolbarComponent,
    ConfirmationDialogComponent,
    FooterRelativeComponent,
    FooterFixedComponent
  ]
})
export class CommonTemplatesModule {
}
