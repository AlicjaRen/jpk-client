export class Record {
  recordId: number;
  monthlyReckoningId: number;
  sumNetHigh: number;
  sumNetMedium: number;
  sumNetLow: number;
  sumVatHigh: number;
  sumVatMedium: number;
  sumVatLow: number;
  sumGross: number;
}
