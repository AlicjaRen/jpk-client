import {Record} from './record';
import {SaleInvoice} from '../../invoices/sale-invoices/model/sale-invoice';

export class SaleRecord extends Record {
  invoices: SaleInvoice[];
}
