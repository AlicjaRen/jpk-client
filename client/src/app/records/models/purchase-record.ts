import {Record} from './record';
import {PurchaseInvoice} from '../../invoices/purchase-invoices/model/purchase-invoice';

export class PurchaseRecord extends Record {
  invoices: PurchaseInvoice[];
}
