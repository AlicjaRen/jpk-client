import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {SaleRecord} from '../models/sale-record';
import {tap} from 'rxjs/operators';
import {PurchaseRecord} from '../models/purchase-record';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = `${environment.API_URL}/record`;

@Injectable({
  providedIn: 'root'
})
export class RecordService {

  constructor(private http: HttpClient) {
  }

  getSaleRecord(id: number): Observable<SaleRecord> {
    const url = `${apiUrl}/sale/${id}`;
    return this.http.get<SaleRecord>(url, httpOptions).pipe(
      tap(() => console.log(`fetched sale record`)));
  }

  getPurchaseRecord(id: number): Observable<PurchaseRecord> {
    const url = `${apiUrl}/purchase/${id}`;
    return this.http.get<PurchaseRecord>(url, httpOptions).pipe(
      tap(() => console.log(`fetched purchase record`)));
  }
}
