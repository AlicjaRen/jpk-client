import {Component, OnInit} from '@angular/core';
import {ReckoningItem} from '../../monthly-reckoning/models/reckoning-item.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ReckoningService} from '../../monthly-reckoning/services/reckoning.service';
import {MatSnackBar, MatTableDataSource} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RecordService} from '../services/record.service';
import {PurchaseInvoice} from '../../invoices/purchase-invoices/model/purchase-invoice';
import {SaleInvoice} from '../../invoices/sale-invoices/model/sale-invoice';
import {Record} from '../models/record';
import {TaxRate} from '../../invoices/shared/models/tax-rate.enum';

@Component({
  selector: 'app-record-details',
  templateUrl: './record-details.component.html',
  styleUrls: ['./record-details.component.scss']
})
export class RecordDetailsComponent implements OnInit {

  componentTitle = 'Rejestr VAT';
  reckoningForm: FormGroup;
  reckoningNameItems: ReckoningItem[];
  currentReckoning: ReckoningItem;
  isSaleRecord = true;
  purchaseInvoices: PurchaseInvoice[];
  saleInvoices: SaleInvoice[];
  reckoningToDisplayId: number;

  records: Record[] = [];
  data: MatTableDataSource<Record>;
  displayedColumns: string[] = ['netLow', 'vatLow', 'netMedium', 'vatMedium', 'netHigh', 'vatHigh', 'net', 'vat', 'gross'];
  taxRate = TaxRate;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private reckoningService: ReckoningService,
    private recordService: RecordService,
    private snackBar: MatSnackBar,
  ) {
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      if (params['recordId']) {
        this.reckoningToDisplayId = params['recordId'];
      }
      console.log(`RecordId from params: ${this.reckoningToDisplayId}`);
    });
    if (this.router.url.includes('sale')) {
      console.log(`Request for sale record`);
      this.isSaleRecord = true;
    } else if (this.router.url.includes('purchase')) {
      console.log(`Request for purchase record`);
      this.isSaleRecord = false;
    }

    this.reckoningService.getReckoningNames().subscribe(result => {
        console.log(`Get reckoning with suggested reckoning name: ${result.currentSuggestedReckoning.name}`);
        if (this.reckoningToDisplayId) {
          this.currentReckoning = result.reckonings.find(value => value.id.toString() === this.reckoningToDisplayId.toString());
          console.log(`Current reckoning: ${this.currentReckoning.name}`);
        } else {
          this.currentReckoning = result.currentSuggestedReckoning;
          this.reckoningToDisplayId = result.currentSuggestedReckoning.id;
        }
        this.reckoningNameItems = result.reckonings;
        this.reckoningForm.controls.reckoningName.setValue(this.currentReckoning);
        this.displayRecord();
      },
      err => {
        console.log(`Error by getting reckonings: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });

    this.reckoningForm = this.formBuilder.group({
      reckoningName: ['', Validators.required]
    });
  }

  compareFn(reckoning1: ReckoningItem, reckoning2: ReckoningItem): boolean {
    return reckoning1 && reckoning2 ? reckoning1.id === reckoning2.id : reckoning1 === reckoning2;
  }

  onTabChange() {
    this.isSaleRecord = !this.isSaleRecord;
    console.log(`Change tab for isSale = ${this.isSaleRecord}`);
    this.displayRecord();
  }

  onAddNewInvoiceClick() {
    if (this.isSaleRecord) {
      this.router.navigate(['/invoice/sale/sale-add']);
    } else {
      this.router.navigate(['/invoice/purchase/purchase-add']);
    }
  }

  onChangeSelectedReckoning() {
    if (this.reckoningForm.value.reckoningName.id) {
      this.reckoningToDisplayId = this.reckoningForm.value.reckoningName.id;
    }
    this.displayRecord();
  }

  private displayRecord() {
    const recordId: number = this.reckoningToDisplayId;

    if (recordId && this.isSaleRecord) {
      console.log(`Displaying sale record with id: ${recordId}`);
      this.recordService.getSaleRecord(recordId).subscribe(result => {
        console.log(`get sale record with ${result.invoices.length} invoices`);
        this.saleInvoices = result.invoices;
        this.records[0] = result;
        this.data = new MatTableDataSource<Record>(this.records);
      }, err => {
        console.log(`Error by getting sale record: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });

    } else if (recordId && !this.isSaleRecord) {
      console.log(`Displaying purchase record with id: ${recordId}`);
      this.recordService.getPurchaseRecord(recordId).subscribe(result => {
        console.log(`get purchase record with ${result.invoices.length} invoices`);
        this.purchaseInvoices = result.invoices;
        this.records[0] = result;
        this.data = new MatTableDataSource<Record>(this.records);
      }, err => {
        console.log(`Error by getting purchase record: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
    }
  }

  countSumNet(): number {
    const record: Record = this.records[0];
    return Math.round((record.sumNetLow + record.sumNetMedium + record.sumNetHigh) * 100) / 100;
  }

  countSumVat(): number {
    const record: Record = this.records[0];
    return Math.round((record.sumVatLow + record.sumVatMedium + record.sumVatHigh) * 100) / 100;
  }

  findSelectedTabIndex(): number {
    return this.isSaleRecord ? 0 : 1;
  }
}
