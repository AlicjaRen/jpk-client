import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecordDetailsComponent} from '../record-details/record-details.component';
import {TaxpayerRoleGuard} from '../../security/guards/taxpayer-role-guard';

const routes: Routes = [
  {path: '', component: RecordDetailsComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'sale', component: RecordDetailsComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'purchase', component: RecordDetailsComponent, canActivate: [TaxpayerRoleGuard]},
  {path: ':recordId', component: RecordDetailsComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'sale/:recordId', component: RecordDetailsComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'purchase/:recordId', component: RecordDetailsComponent, canActivate: [TaxpayerRoleGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecordsRoutingModule {
}
