import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RecordDetailsComponent} from './record-details/record-details.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {CommonTemplatesModule} from '../common-templates/common-templates.module';
import {RecordsRoutingModule} from './routes/records-routing.module';
import {SaleInvoicesModule} from '../invoices/sale-invoices/sale-invoices.module';
import {PurchaseInvoicesModule} from '../invoices/purchase-invoices/purchase-invoices.module';

@NgModule({
  imports: [
    CommonModule,
    RecordsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    CommonTemplatesModule,
    SaleInvoicesModule,
    PurchaseInvoicesModule
  ],
  declarations: [
    RecordDetailsComponent
  ]
})
export class RecordsModule {
}
