import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuditFileListComponent} from './audit-file-list/audit-file-list.component';
import {AuditFilesRoutingModule} from './routes/audit-files-routing.module';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {CommonTemplatesModule} from '../common-templates/common-templates.module';
import {ConfirmationDialogComponent} from '../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    AuditFileListComponent
  ],
  imports: [
    CommonModule,
    AuditFilesRoutingModule,
    AngularMaterialModule,
    CommonTemplatesModule
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ]
})
export class AuditFilesModule {
}
