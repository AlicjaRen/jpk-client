import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AuditFileListComponent} from './audit-file-list.component';

describe('AuditFileListComponent', () => {
  let component: AuditFileListComponent;
  let fixture: ComponentFixture<AuditFileListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AuditFileListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditFileListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
