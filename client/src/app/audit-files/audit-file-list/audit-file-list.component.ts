import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {AuditFileService} from '../services/audit-file.service';
import {ReckoningService} from '../../monthly-reckoning/services/reckoning.service';
import {ReckoningItem} from '../../monthly-reckoning/models/reckoning-item.model';
import {AuditFileCorrection} from '../models/audit-file-correction';
import {AuditFileCorrectionService} from '../services/audit-file-correction.service';
import {ConfirmationDialogComponent} from '../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-audit-file-list',
  templateUrl: './audit-file-list.component.html',
  styleUrls: ['./audit-file-list.component.scss']
})
export class AuditFileListComponent implements OnInit {

  componentTitle = 'Jednolite pliki kontrolne VAT';

  reckoningItems: ReckoningItem[];
  data: MatTableDataSource<ReckoningItem>;
  displayedColumns: string[] = ['name', 'auditFileId', 'auditFileCorrections'];

  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;


  constructor(
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private reckoningService: ReckoningService,
    private auditFileService: AuditFileService,
    private auditFileCorrectionService: AuditFileCorrectionService
  ) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.refreshAuditFileList();
  }

  refreshAuditFileList() {
    this.reckoningService.getReckoningNames().subscribe(result => {
        console.log(`Get reckoning items`);
        this.reckoningItems = result.reckonings;
        this.data = new MatTableDataSource<ReckoningItem>(this.reckoningItems);
        this.data.paginator = this.paginator;
        this.data.sort = this.sort;
      },
      err => {
        console.log(`Error by getting reckonings: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
  }

  onGenerateAuditFileClick(reckoningItem: ReckoningItem) {
    console.log(`Generate SAF-T for monthly reckoning with id: ${reckoningItem.id} click`);
    this.auditFileService.create(reckoningItem.id).subscribe(result => {
        console.log(`Audit file with id: ${result.auditFileId} created`);
        this.refreshAuditFileList();
        this.snackBar.open(`Wygenerowano JPK dla rozliczenia: ${reckoningItem.name}`, 'Zamknij', {duration: 10000});
      },
      err => {
        console.log(`Error by generating SAF-T for reckoning with id: ${reckoningItem.id}: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
  }

  onAuditFileClick(reckoningItem: ReckoningItem) {
    console.log(`Download request for SAF-T with id: ${reckoningItem.auditFileId}`);
    this.auditFileService.downloadFile(reckoningItem.auditFileId).subscribe(result => {
        this.downloadFile(result);
      },
      err => {
        console.log(`Error by downloading audit file with id: ${reckoningItem.auditFileId}: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
  }

  downloadFile(result: HttpResponse<any>) {
    const blob = new Blob([result.body], {type: 'application/xml'});

    // for IE
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob);
      return;
    }

    const data = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = data;

    const contentDisposition = result.headers.get('Content-Disposition');
    const stringsFromHeader = contentDisposition.split('=');
    link.download = stringsFromHeader[1].replace(/"/g, '');
    link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));

    setTimeout(function () {
      // for Firefox
      window.URL.revokeObjectURL(data);
      link.remove();
    }, 100);
  }

  onGenerateAgainAuditFileClick(reckoningItem: ReckoningItem) {
    console.log(`Again generate SAF-T with id: ${reckoningItem.auditFileId} click`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Ponowne generowanie JPK',
        message: `JPK dla rozliczenia: ${reckoningItem.name} zostanie wygenerowany ponownie. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.generateAgainAuditFile(reckoningItem);
      }
    });
  }

  generateAgainAuditFile(reckoningItem: ReckoningItem) {
    this.auditFileService.update(reckoningItem.auditFileId).subscribe(result => {
        console.log(`Updated audit file with id: ${result.auditFileId}`);
        this.snackBar.open(`Wygenerowano ponownie JPK dla rozliczenia: ${reckoningItem.name}`, 'Zamknij', {duration: 10000});
      },
      err => {
        console.log(`Error by generating again SAF-T with id: ${reckoningItem.auditFileId}: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
  }

  onDeleteAuditFileClick(reckoningItem: ReckoningItem) {
    console.log(`Delete for SAF-T with id: ${reckoningItem.auditFileId} click`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Usuwanie JPK',
        message: `JPK dla rozliczenia: ${reckoningItem.name} zostanie usunięty. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.deleteAuditFile(reckoningItem);
      }
    });
  }

  deleteAuditFile(reckoningItem: ReckoningItem) {
    this.auditFileService.delete(reckoningItem.auditFileId).subscribe(() => {
        console.log(`Audit file with id: ${reckoningItem.auditFileId} deleted`);
        this.refreshAuditFileList();
        this.snackBar.open(`Usunięto JPK dla rozliczenia: ${reckoningItem.name}`, 'Zamknij', {duration: 10000});
      },
      err => {
        console.log(`Error by deleting audit file with id: ${reckoningItem.auditFileId}: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
  }

  onGenerateAuditFileCorrectionClick(reckoningItem: ReckoningItem) {
    console.log(`Generate correction for monthly reckoning with id: ${reckoningItem.id}`);
    this.auditFileCorrectionService.create(reckoningItem.id).subscribe(result => {
        console.log(`Audit file correction with id: ${result.auditFileId} created`);
        this.refreshAuditFileList();
        this.snackBar.open(`Wygenerowano korektę JPK dla rozliczenia: ${reckoningItem.name}`, 'Zamknij', {duration: 10000});
      },
      err => {
        console.log(`Error by generating SAF-T correction for reckoning with id: ${reckoningItem.id}: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
  }

  onAuditFileCorrectionClick(correction: AuditFileCorrection) {
    console.log(`Download request for correction with id: ${correction.auditFileId}`);
    this.auditFileCorrectionService.downloadFile(correction.auditFileId).subscribe(result => {
        this.downloadFile(result);
      },
      err => {
        console.log(`Error by downloading audit file correction with id: ${correction.auditFileId}: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
  }

  onGenerateAgainAuditFileCorrectionClick(correction: AuditFileCorrection, reckoningItem: ReckoningItem) {
    console.log(`Again generate correction with id: ${correction.auditFileId}`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Ponowne generowanie korekty JPK',
        message: `Koretka JPK o nazwie ${correction.fileName} dla rozliczenia: ${reckoningItem.name}
        zostanie wygenerowana ponownie. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.generateAgainAuditFileCorrection(correction, reckoningItem);
      }
    });
  }

  private generateAgainAuditFileCorrection(correction: AuditFileCorrection, reckoningItem: ReckoningItem) {
    this.auditFileCorrectionService.update(correction.auditFileId).subscribe(result => {
        console.log(`Updated audit file correction with id: ${result.auditFileId}`);
        this.snackBar.open(`Wygenerowano ponownie korektę JPK o nazwie ${correction.fileName} dla rozliczenia:
        ${reckoningItem.name}`, 'Zamknij', {duration: 10000});
      },
      err => {
        console.log(`Error by generating again SAF-T with id: ${correction.auditFileId}: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
  }

  onDeleteAuditFileCorrectionClick(correction: AuditFileCorrection, reckoningItem: ReckoningItem) {
    console.log(`Delete for correction with id: ${correction.auditFileId}`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Usuwanie korekty JPK',
        message: `Korekta JPK o nazwie ${correction.fileName} dla rozliczenia: ${reckoningItem.name}
        zostanie usunięta. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.deleteAuditFileCorrection(correction, reckoningItem);
      }
    });
  }

  private deleteAuditFileCorrection(correction: AuditFileCorrection, reckoningItem: ReckoningItem) {
    this.auditFileCorrectionService.delete(correction.auditFileId).subscribe(() => {
        console.log(`Audit file correction with id: ${correction.auditFileId} deleted`);
        this.refreshAuditFileList();
        this.snackBar.open(`Usunięto korektę JPK o nazwie ${correction.fileName} dla rozliczenia:
         ${reckoningItem.name}`, 'Zamknij', {duration: 10000});
      },
      err => {
        console.log(`Error by deleting audit file correction with id: ${correction.auditFileId}: ${err}`);
        this.snackBar.open(`Wystąpił błąd. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
  }
}
