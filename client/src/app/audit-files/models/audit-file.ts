export class AuditFile {
  auditFileId: number;
  fileName: string;
  data: Int8Array;
  monthlyReckoningId: number;
}
