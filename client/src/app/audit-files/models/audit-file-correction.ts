export class AuditFileCorrection {
  auditFileId: number;
  fileName: string;
  numberOfCorrection: number;
  data: Int8Array;
}
