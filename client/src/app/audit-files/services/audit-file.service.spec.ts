import {TestBed} from '@angular/core/testing';

import {AuditFileService} from './audit-file.service';

describe('AuditFileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuditFileService = TestBed.get(AuditFileService);
    expect(service).toBeTruthy();
  });
});
