import {TestBed} from '@angular/core/testing';

import {AuditFileCorrectionService} from './audit-file-correction.service';

describe('AuditFileCorrectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuditFileCorrectionService = TestBed.get(AuditFileCorrectionService);
    expect(service).toBeTruthy();
  });
});
