import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AuditFileCorrection} from '../models/audit-file-correction';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const apiUrl = `${environment.API_URL}/audit/file/correction`;

@Injectable({
  providedIn: 'root'
})
export class AuditFileCorrectionService {

  constructor(private http: HttpClient) {
  }

  create(monthlyReckoningId: number): Observable<AuditFileCorrection> {
    const url = `${apiUrl}/${monthlyReckoningId}`;
    return this.http.post<AuditFileCorrection>(url, httpOptions).pipe(
      tap((createdAuditFile: AuditFileCorrection) =>
        console.log(`created audit file correction with id= ${createdAuditFile.auditFileId}`)));
  }

  downloadFile(auditFileId: number): any {
    const url = `${apiUrl}/${auditFileId}`;
    return this.http.get(url, {responseType: 'text', observe: 'response'});
  }

  update(auditFileId: number): Observable<AuditFileCorrection> {
    const url = `${apiUrl}/${auditFileId}`;
    return this.http.put<AuditFileCorrection>(url, httpOptions).pipe(
      tap((updatedAuditFile: AuditFileCorrection) =>
        console.log(`updated audit file correction with id= ${updatedAuditFile.auditFileId}`)));
  }

  delete(auditFileId: number) {
    console.log(`Deleting audit file correction with id=${auditFileId}`);
    const url = `${apiUrl}/${auditFileId}`;
    return this.http.delete(url);
  }
}
