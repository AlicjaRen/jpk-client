import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AuditFile} from '../models/audit-file';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const apiUrl = `${environment.API_URL}/audit/file`;

@Injectable({
  providedIn: 'root'
})
export class AuditFileService {

  constructor(private http: HttpClient) {
  }

  create(monthlyReckoningId: number): Observable<AuditFile> {
    const url = `${apiUrl}/${monthlyReckoningId}`;
    return this.http.post<AuditFile>(url, httpOptions).pipe(
      tap((createdAuditFile: AuditFile) =>
        console.log(`created audit file with id= ${createdAuditFile.auditFileId}`)));
  }

  downloadFile(auditFileId: number): any {
    const url = `${apiUrl}/${auditFileId}`;
    return this.http.get(url, {responseType: 'text', observe: 'response'});
  }

  update(auditFileId: number): Observable<AuditFile> {
    const url = `${apiUrl}/${auditFileId}`;
    return this.http.put<AuditFile>(url, httpOptions).pipe(
      tap((updatedAuditFile: AuditFile) =>
        console.log(`updated audit file with id= ${updatedAuditFile.auditFileId}`)));
  }

  delete(auditFileId: number) {
    console.log(`Deleting audit file with id=${auditFileId}`);
    const url = `${apiUrl}/${auditFileId}`;
    return this.http.delete(url);
  }
}
