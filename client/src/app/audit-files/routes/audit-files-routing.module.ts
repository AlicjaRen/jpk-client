import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuditFileListComponent} from '../audit-file-list/audit-file-list.component';
import {TaxpayerRoleGuard} from '../../security/guards/taxpayer-role-guard';

const routes: Routes = [
  {path: '', component: AuditFileListComponent, canActivate: [TaxpayerRoleGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuditFilesRoutingModule {
}
