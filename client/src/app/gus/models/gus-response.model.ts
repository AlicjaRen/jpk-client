export class GusResponse {
  regonNumber: string;
  regonLink: string;
  name: string;
  province: string;
  county: string;
  borough: string;
  town: string;
  street: string;
  zipCode: string;
  nip: number;
}
