import {TestBed} from '@angular/core/testing';

import {GusService} from './gus.service';

describe('GusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GusService = TestBed.get(GusService);
    expect(service).toBeTruthy();
  });
});
