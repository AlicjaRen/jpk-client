import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {GusResponse} from '../models/gus-response.model';
import {environment} from '../../../environments/environment';
import {tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class GusService {

  constructor(private http: HttpClient) {
  }

  getDataFromGus(nip: number): Observable<GusResponse> {
    const url = `${environment.API_URL}/gus/nip/${nip}`;
    return this.http.get(url, httpOptions).pipe(
      tap((gusResponse: GusResponse) => console.log(`downloading data from GUS for company: ${gusResponse.name}`)));
  }
}
