import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {tap} from 'rxjs/operators';
import {Reckoning} from '../models/reckoning';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = `${environment.API_URL}/reckoning`;

@Injectable({
  providedIn: 'root'
})
export class ReckoningService {

  constructor(private http: HttpClient) {
  }

  getReckoningNames(): Observable<Reckoning> {
    return this.http.get<Reckoning>(apiUrl, httpOptions).pipe(
      tap(() => console.log(`fetched reckonings names`)));
  }
}
