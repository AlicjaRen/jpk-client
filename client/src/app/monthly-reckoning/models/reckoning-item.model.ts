import {AuditFileCorrection} from '../../audit-files/models/audit-file-correction';

export class ReckoningItem {
  name: string;
  id: number;
  saleRecordId: number;
  purchaseRecordId: number;
  auditFileId: number;
  auditFileName: string;
  auditFileCorrections: Array<AuditFileCorrection>;
}
