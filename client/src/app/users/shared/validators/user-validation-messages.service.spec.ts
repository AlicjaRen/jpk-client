import {TestBed} from '@angular/core/testing';

import {UserValidationMessagesService} from './user-validation-messages.service';

describe('UserValidationMessagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserValidationMessagesService = TestBed.get(UserValidationMessagesService);
    expect(service).toBeTruthy();
  });
});
