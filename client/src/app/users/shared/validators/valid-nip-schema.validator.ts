import {FormGroup} from '@angular/forms';

// checking that last digit of NIP is valid with control sum
export function ValidNIPSchema(controlNIPName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlNIPName];
    if (isNaN(Number(control))) {
      control.setErrors({validNIPSchema: true});
    }
    const weighs = [6, 5, 7, 2, 3, 4, 5, 6, 7];
    const stringNIP = control.value.toString();
    let sum = 0;
    for (let i = 0; i < 9; i++) {
      sum += weighs[i] * stringNIP.charAt(i);
    }
    console.log(`Sum for NIP control: ${sum}`);
    if (sum % 11 === parseInt(stringNIP.charAt(9), 10)) {
      control.setErrors(null);
    } else {
      control.setErrors({validNIPSchema: true});
    }
  };
}
