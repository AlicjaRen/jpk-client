import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserValidationMessagesService {

  constructor() {
  }

  user_validation_messages = {
    'NIP': [
      {type: 'required', message: 'Wymagane 10 cyfr'},
      {type: 'pattern', message: 'Wymagane 10 cyfr'},
      {type: 'validNIPSchema', message: 'Błędny NIP - wymagane 10 cyfr wraz z prawidłową cyfra kontrolną'},
    ],
    'name': [
      {type: 'required', message: 'Pole wymagane'}
    ],
    'email': [
      {type: 'required', message: 'Pole wymagane'},
      {type: 'email', message: 'Nieprawidłowa postać adresu email'}
    ],
    'voivodeship': [
      {type: 'required', message: 'Pole wymagane'}
    ],
    'county': [
      {type: 'required', message: 'Pole wymagane'}
    ],
    'zipCode': [
      {type: 'required', message: 'Pole wymagane'},
      {type: 'pattern', message: 'Wymagana postać: NN-NNN'}
    ],
    'borough': [
      {type: 'required', message: 'Pole wymagane'}
    ],
    'town': [
      {type: 'required', message: 'Pole wymagane'}
    ],
    'buildingNumber': [
      {type: 'required', message: 'Pole wymagane'}
    ],
    'login': [
      {type: 'required', message: 'Pole wymagane'}
    ],
    'password': [
      {type: 'required', message: 'Pole wymagane'},
      {type: 'minLength', message: 'Wymagana długość hasła min 8 znaków'},
      {
        type: 'pattern', message: 'Min 8 znaków, w tym: min 1 mała litera, 1 wielka litera, 1 cyfra, 1 znak specjalny spośród:' +
          ' @#$%^&+=!*(){}[]_|<>?'
      }
    ],
    'passwordConfirmation': [
      {type: 'required', message: 'Pole wymagane'},
      {type: 'mustMatch', message: 'Powtórzenie hasła musi być zgodne z hasłem'}
    ],
    'oldPassword': [
      {type: 'required', message: 'Pole wymagane'},
      {type: 'minLength', message: 'Wymagana długość hasła min 8 znaków'},
      {
        type: 'pattern', message: 'Min 8 znaków, w tym: min 1 mała litera, 1 wielka litera, 1 cyfra, 1 znak specjalny spośród:' +
          ' @#$%^&+=!*(){}[]_|<>?'
      }
    ],
    'newPassword': [
      {type: 'required', message: 'Pole wymagane'},
      {type: 'minLength', message: 'Wymagana długość hasła min 8 znaków'},
      {
        type: 'pattern', message: 'Min 8 znaków, w tym: min 1 mała litera, 1 wielka litera, 1 cyfra, 1 znak specjalny spośród:' +
          ' @#$%^&+=!*(){}[]_|<>?'
      }
    ],
    'newPasswordConfirmation': [
      {type: 'required', message: 'Pole wymagane'},
      {type: 'mustMatch', message: 'Powtórzenie hasła musi być zgodne z hasłem'}
    ]
  };
}
