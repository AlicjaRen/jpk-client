import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserValidationMessagesService} from '../validators/user-validation-messages.service';
import {MatSnackBar} from '@angular/material';
import {MustMatch} from '../validators/must-match.validator';
import {UserAccountService} from '../services/user-account.service';
import {AuthenticationService} from '../../../security/services/authentication.service';
import {UserRole} from '../models/user-role.enum';

@Component({
  selector: 'app-password-change',
  templateUrl: './password-change.component.html',
  styleUrls: ['./password-change.component.scss']
})
export class PasswordChangeComponent implements OnInit {
  changePasswordForm: FormGroup;
  oldPasswordHide = true;
  newPasswordHide = true;
  newPasswordConfirmationHide = true;
  errorMessage: string;
  userId: number;
  componentTitle = 'Zmiana hasła';

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public messages: UserValidationMessagesService,
              private userAccountService: UserAccountService,
              private authenticationService: AuthenticationService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.userId = Number(this.route.snapshot.paramMap.get('userId'));

    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', [Validators.required, Validators.minLength(8),
        Validators.pattern('^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!*(){}\\[\\]\\-_|<>?])(?=\\S+$).{8,}$')]],
      newPassword: ['', [Validators.required, Validators.minLength(8),
        Validators.pattern('^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!*(){}\\[\\]\\-_|<>?])(?=\\S+$).{8,}$')]],
      newPasswordConfirmation: ['', Validators.required],
    }, {
      passwordConfirmValidator: MustMatch('newPassword', 'newPasswordConfirmation')
    });
  }


  // getter for easy access to form fields
  get form() {
    return this.changePasswordForm.controls;
  }

  onUndoClick() {
    console.log('Undo click');
    this.navigateToUserProfile();
  }

  private navigateToUserProfile() {
    if (UserRole.ADMIN === UserRole [this.authenticationService.currentUserValue.userRole]) {
      this.router.navigate([`/user/admin/admin-details/${this.userId}`]);
    } else {
      this.router.navigate([`/user/taxpayer/taxpayer-details/${this.userId}`]);
    }
  }

  onSubmit(form: NgForm) {
    console.log('Send change password form');
    this.userAccountService.changePassword(form).subscribe(() => {
        this.navigateToUserProfile();
        this.snackBar.open('Hasło zostało pomyślnie zmienione', 'Zamknij', {duration: 10000});
      },
      () => {
        this.snackBar.open('Zmiana hasła nie powiodła się', 'Zamknij', {duration: 10000});
      });
  }


}
