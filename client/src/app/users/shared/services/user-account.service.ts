import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {ChangePassword} from '../models/change-password.model';
import {tap} from 'rxjs/operators';
import {ResetPassword} from '../models/reset-password.model';

const apiUrl = `${environment.API_URL}/user/password`;

@Injectable({
  providedIn: 'root'
})
export class UserAccountService {

  constructor(private http: HttpClient) {
  }

  changePassword(changePasswordRequest: any): Observable<ChangePassword> {
    return this.http.put<ChangePassword>(apiUrl, changePasswordRequest).pipe(
      tap(() => console.log('Changed password')));
  }

  resetPassword(userId: any): Observable<ResetPassword> {
    const url = `${apiUrl}/reset/${userId}`;
    return this.http.put<ResetPassword>(url, null).pipe(
      tap(() => console.log(`Reset password for user with id: ${userId}`)));
  }
}
