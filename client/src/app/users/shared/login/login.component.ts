import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '../../../security/services/authentication.service';
import {first} from 'rxjs/operators';
import {UserRole} from '../models/user-role.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  passwordHide = true;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService,
  ) {
    // redirect logged in user to home
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });

    // returnUrl from route parameters (if it's other than login page route) or default to home: '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // getter for easy access to form fields
  get form() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    console.log('Submitting login form');

    // stop if form is invalid (run Validators)
    if (this.loginForm.invalid) {
      console.log('Login form invalid, login: %s, password: %s', this.form.login.value, this.form.password.value);
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.form.login.value, this.form.password.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log('correct login');
          if (UserRole.ADMIN === UserRole[data.userRole]) {
            console.log('Logged admin');
            this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home/admin';
          } else {
            console.log('Logged taxpayer');
            this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/home/taxpayer';
          }
          this.router.navigate([this.returnUrl]);
        },
        error => {
          console.log('Error by login');
          this.errorMessage = error;
          this.loading = false;
        });
  }

}
