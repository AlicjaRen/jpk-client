import {UserRole} from './user-role.enum';

export class User {
  login: string;
  token: string;
  userId: number;
  userRole: UserRole;
  password: string;
}
