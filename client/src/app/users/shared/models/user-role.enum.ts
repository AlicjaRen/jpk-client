export enum UserRole {
  ADMIN = 'ADMIN',
  TAXPAYER = 'TAXPAYER'
}
