export class ResetPassword {
  userId: number;
  login: string;
  newPassword: string;
}
