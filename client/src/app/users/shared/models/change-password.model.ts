export class ChangePassword {
  oldPassword: string;
  newPassword: string;
  newPasswordConfirmation: string;
}
