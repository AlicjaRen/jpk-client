import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LoginComponent} from './shared/login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {UsersRoutingModule} from './routes/users-routing.module';
import {CommonTemplatesModule} from '../common-templates/common-templates.module';
import {SimplealertModule} from 'simplealert';
import {PasswordChangeComponent} from './shared/password-change/password-change.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    UsersRoutingModule,
    CommonTemplatesModule,
    SimplealertModule
  ],
  declarations: [LoginComponent, PasswordChangeComponent]
})
export class UsersModule {
}
