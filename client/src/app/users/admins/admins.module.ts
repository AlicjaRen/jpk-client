import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminComponent} from './admin/admin.component';
import {AdminListComponent} from './admin-list/admin-list.component';
import {AdminsRoutingModule} from './routes/admins-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../angular-material/angular-material.module';
import {CommonTemplatesModule} from '../../common-templates/common-templates.module';
import {ConfirmationDialogComponent} from '../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';
import {AdminAddComponent} from './admin-add/admin-add.component';

@NgModule({
  imports: [
    CommonModule,
    AdminsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    CommonTemplatesModule
  ],
  declarations: [
    AdminComponent,
    AdminListComponent,
    AdminAddComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ]
})
export class AdminsModule {
}
