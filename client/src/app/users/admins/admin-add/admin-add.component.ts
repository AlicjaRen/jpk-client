import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AdminsService} from '../services/admins.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-admin-add',
  templateUrl: './admin-add.component.html',
  styleUrls: ['./admin-add.component.scss']
})
export class AdminAddComponent implements OnInit {

  componentTitle = 'Rejestracja nowego administratora';
  registerForm: FormGroup;


  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private adminService: AdminsService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  get form() {
    return this.registerForm.controls;
  }

  onSubmit(form: NgForm) {
    console.log('Send new admin register form');
    this.adminService.create(form).subscribe(result => {
      this.router.navigate([`/user/admin`]);
      this.snackBar.open(`Pomyślnie zarejestrowano nowego administratora o loginie: ${result.login} z nadanym hasłem:
       ${result.password}`, 'Zamknij');
    }, error1 => {
      console.log(`Error by creating new admin: ${error1}`);
      this.snackBar.open(`Dodanie nowego administratora o loginie: ${form['email']} nie powiodło się: ${error1}`,
        'Zamknij', {duration: 10000});
    });
  }
}
