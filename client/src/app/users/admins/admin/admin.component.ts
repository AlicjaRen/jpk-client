import {Component, OnInit} from '@angular/core';
import {Admin} from '../models/admin.model';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AdminsService} from '../services/admins.service';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {AuthenticationService} from '../../../security/services/authentication.service';
import {ConfirmationDialogComponent} from '../../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';
import {UserAccountService} from '../../shared/services/user-account.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  componentTitle = 'Profil administratora';
  cardTitle: string;
  readMode = true;
  thisAdmin: boolean;
  admin: Admin;
  adminProfileForm: FormGroup;
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private adminService: AdminsService,
              private authenticationService: AuthenticationService,
              private userAccountService: UserAccountService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    const adminId: number = Number(this.route.snapshot.paramMap.get('adminId'));
    console.log(`Read request for admin with id: ${adminId}`);
    this.thisAdmin = this.authenticationService.currentUserValue.userId === adminId;
    this.adminService.read(adminId).subscribe(response => {
        this.admin = response;
        this.fillAdminForm();
      },
      error1 => {
        console.log(`Error by getting admin: ${error1}`);
        this.snackBar.open(`Wystąpił błąd podczas podbierania danych administratora: ${error1}`, 'Zamknij', {duration: 10000});
      });

    if (this.router.url.includes('edit')) {
      console.log(`Edit mode`);
      this.cardTitle = 'Edycja danych';
      this.readMode = false;
    } else {
      console.log(`Read mode`);
      this.cardTitle = this.thisAdmin ? 'Twoje dane' : 'Dane administratora';
      this.readMode = true;
    }

    this.adminProfileForm = this.formBuilder.group({
      login: [{value: '', disabled: this.readMode}, Validators.required],
      email: [{value: '', disabled: this.readMode}, [Validators.required, Validators.email]],
      userId: null
    });
  }

  get form() {
    return this.adminProfileForm.controls;
  }

  private fillAdminForm() {
    this.form.login.setValue(this.admin.login);
    this.form.email.setValue(this.admin.email);
    this.form.userId.setValue(this.admin.userId);
  }

  onEditClick() {
    console.log('Edit click');
    this.router.navigate([`/user/admin/admin-edit/${this.admin.userId}`]);
  }

  onChangePasswordClick() {
    this.router.navigate([`/user/password-change/${this.admin.userId}`]);
  }

  onSubmit(form: NgForm) {
    console.log(`Send edit admin data`);
    this.adminService.update(this.admin.userId, form).subscribe(result => {
      this.navigateAfterProfileUpdate(result);
    }, error1 => {
      console.log(`Error by update admin profile: ${error1}`);
      this.snackBar.open(`Aktualizacja danych nie powiodła się`, 'Zamknij', {duration: 10000});
    });
  }

  private navigateAfterProfileUpdate(result: Admin) {
    if (result.userId === this.authenticationService.currentUserValue.userId) {
      this.authenticationService.logout();
      this.router.navigate([`/user/login`]);
      this.snackBar.open(`Dane zostały pomyślnie zaktualizowane. Prosimy o ponowne zalogowanie.`, 'Zamknij', {duration: 10000});
    } else {
      this.router.navigate(['/user/admin']);
      this.snackBar.open(`Dane administratora o loginie: ${result.login} zostały pomyślnie zaktualizowane`, 'Zamknij', {duration: 10000});
    }
  }

  onResetPasswordClick() {
    console.log(`Reset password click for admin with id: ${this.admin.userId}`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Resetowanie hasła użytkownika',
        message: `Hasło użytkownika o loginie: ${this.admin.login} zostanie zresetowane. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.resetPassword(this.admin.userId);
      }
    });
  }

  private resetPassword(userId: number) {
    this.userAccountService.resetPassword(userId).subscribe(result => {
        this.snackBar.open(`Pomyślnie zresetowano hasło dla użytkownika o id: ${result.userId} i loginie: ${result.login}.
       \n Nowe hasło: ${result.newPassword}`, 'Zamknij');
      },
      error1 => {
        console.log(`Error by reset password for user with id: ${userId}: ${error1}`);
        this.snackBar.open('Resetowanie hasła nie powiodło się', 'Zamknij', {duration: 10000});
      });
  }
}
