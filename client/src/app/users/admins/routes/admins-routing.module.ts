import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from '../admin/admin.component';
import {AdminRoleGuard} from '../../../security/guards/admin-role.guard';
import {AdminListComponent} from '../admin-list/admin-list.component';
import {AdminAddComponent} from '../admin-add/admin-add.component';

const routes: Routes = [
  {path: 'admin-add', component: AdminAddComponent, canActivate: [AdminRoleGuard]},
  {path: 'admin-details/:adminId', component: AdminComponent, canActivate: [AdminRoleGuard]},
  {path: 'admin-edit/:adminId', component: AdminComponent, canActivate: [AdminRoleGuard]},
  {path: '**', component: AdminListComponent, canActivate: [AdminRoleGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminsRoutingModule {
}
