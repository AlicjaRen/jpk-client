import {User} from '../../shared/models/user.model';

export class Admin extends User {
  email: string;
}
