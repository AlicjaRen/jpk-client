import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Admin} from '../models/admin.model';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = `${environment.API_URL}/user/admin`;

@Injectable({
  providedIn: 'root'
})
export class AdminsService {

  constructor(private http: HttpClient) {
  }

  create(admin: any): Observable<Admin> {
    const url = `${apiUrl}`;
    return this.http.post<Admin>(url, admin, httpOptions).pipe(
      tap((createdAdmin: Admin) => console.log(`created admin with id= ${createdAdmin.userId}`)));
  }

  read(id: number): Observable<Admin> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Admin>(url).pipe(
      tap(() => console.log(`fetched admin with id=${id}`)));
  }

  getAll(): Observable<Admin[]> {
    return this.http.get<Admin[]>(`${apiUrl}`).pipe(
      tap(() => console.log('fetched admins')));
  }

  update(id, admin): Observable<Admin> {
    const url = `${apiUrl}/${id}`;
    return this.http.put<Admin>(url, admin).pipe(
      tap((updatedAdmin: Admin) => console.log(`updated admin with id=${updatedAdmin.userId}`)));
  }

  delete(id: number): Observable<Admin> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Admin>(url).pipe(
      tap(() => console.log(`deleted admin with id=${id}`)));
  }
}
