import {Component, OnInit, ViewChild} from '@angular/core';
import {Admin} from '../models/admin.model';
import {MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {ConfirmationDialogComponent} from '../../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';
import {Router} from '@angular/router';
import {AdminsService} from '../services/admins.service';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.scss']
})
export class AdminListComponent implements OnInit {

  componentTitle = 'Lista administratorów zarejestrowanych w systemie';
  admins: Admin[];
  data: MatTableDataSource<Admin>;
  displayedColumns: string[] = ['no', 'userId', 'login', 'email', 'editButton', 'deleteButton'];
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(private router: Router,
              private adminService: AdminsService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.refreshAdminsList();
  }

  refreshAdminsList() {
    this.adminService.getAll().subscribe(result => {
      this.admins = result;
      this.data = new MatTableDataSource<Admin>(this.admins);
      this.data.paginator = this.paginator;
      this.data.sort = this.sort;
    }, error1 => {
      console.log(`Error by getting all admins: ${error1}`);
      this.snackBar.open('Wystąpił błąd podczas pobierania listy administratorów', 'Zamknij', {duration: 10000});
    });
  }

  onRowClick(admin: Admin) {
    console.log(`Click on admin with id: ${admin.userId}`);
    this.router.navigate([`/user/admin/admin-details/${admin.userId}`]);
  }

  onEditClick(admin: Admin) {
    console.log(`Edit click on admin with id: ${admin.userId}`);
    this.router.navigate([`/user/admin/admin-edit/${admin.userId}`]);
  }

  onDeleteClick(admin: Admin) {
    console.log(`Delete click on admin with id: ${admin.userId}`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Usuwanie administratora',
        message: `Konto administratora o loginie: ${admin.login} zostanie usunięte. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.deleteAdmin(admin);
      }
    });
  }

  private deleteAdmin(admin: Admin) {
    this.adminService.delete(admin.userId).subscribe(() => {
        this.snackBar.open(`Pomyślnie usunięto administratora o loginie ${admin.login}`, 'Zamknij', {duration: 10000});
        this.refreshAdminsList();
      },
      error1 => {
        console.log(`Error by deleting admin with id: ${admin.userId}: ${error1}`);
        this.snackBar.open(`Usuwanie administratora o loginie: ${admin.login} nie powiodło się`, 'Zamknij', {duration: 10000});
      });
  }

  onCreateAdminClick() {
    console.log('Create new admin click');
    this.router.navigate(['/user/admin/admin-add']);
  }
}
