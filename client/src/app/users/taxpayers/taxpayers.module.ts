import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaxpayerAddComponent} from './taxpayer-add/taxpayer-add.component';
import {TaxpayerDetailsComponent} from './taxpayer-details/taxpayer-details.component';
import {TaxpayerListComponent} from './taxpayer-list/taxpayer-list.component';
import {TaxpayersRoutingModule} from './routes/taxpayers-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../angular-material/angular-material.module';
import {CommonTemplatesModule} from '../../common-templates/common-templates.module';
import {ConfirmationDialogComponent} from '../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    TaxpayersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    CommonTemplatesModule
  ],
  declarations: [
    TaxpayerAddComponent,
    TaxpayerDetailsComponent,
    TaxpayerListComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ]
})
export class TaxpayersModule {
}
