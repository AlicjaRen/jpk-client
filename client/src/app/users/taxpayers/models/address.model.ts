export class Address {
  addressId: number;
  voivodeship: string;
  county: string;
  zipCode: string;
  borough: string;
  town: string;
  street: string;
  buildingNumber: string;
}
