import {User} from '../../shared/models/user.model';
import {Address} from './address.model';

export class Taxpayer extends User {
  email: string;
  address: Address;
  nip: number;
  name: string;
}
