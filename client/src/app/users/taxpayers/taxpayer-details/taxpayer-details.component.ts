import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Taxpayer} from '../models/taxpayer.model';
import {UserValidationMessagesService} from '../../shared/validators/user-validation-messages.service';
import {MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {TaxpayersService} from '../services/taxpayers.service';
import {GusService} from '../../../gus/services/gus.service';
import {AuthenticationService} from '../../../security/services/authentication.service';
import {UserRole} from '../../shared/models/user-role.enum';
import {ConfirmationDialogComponent} from '../../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';
import {UserAccountService} from '../../shared/services/user-account.service';
import {ValidNIPSchema} from '../../shared/validators/valid-nip-schema.validator';

@Component({
  selector: 'app-taxpayer-details',
  templateUrl: './taxpayer-details.component.html',
  styleUrls: ['./taxpayer-details.component.scss']
})
export class TaxpayerDetailsComponent implements OnInit {

  componentTitle = 'Profil podatnika';
  cardTitle: string;
  readMode = true;
  isLoadingDataFromGUS = false;
  errorMessage: string;
  taxpayer: Taxpayer;
  taxpayerProfileForm: FormGroup;
  addressForm: FormGroup;
  usingByAdmin: boolean;
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              public messages: UserValidationMessagesService,
              private gusService: GusService,
              private taxpayerService: TaxpayersService,
              private authenticationService: AuthenticationService,
              private userAccountService: UserAccountService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    const currentUser = this.authenticationService.currentUserValue;
    this.usingByAdmin = UserRole.ADMIN === UserRole [currentUser.userRole];
    const taxpayerId: number = Number(this.route.snapshot.paramMap.get('taxpayerId'));
    console.log(`Request for taxpayer with id: ${taxpayerId}`);
    this.taxpayerService.read(taxpayerId)
      .subscribe(
        response => {
          this.taxpayer = response;
          this.fillTaxpayerForm();
          this.errorMessage = '';
          console.log(`Getting data for taxpayer with id: ${this.taxpayer.userId}`);
        },
        error1 => {
          console.log(`Error by getting taxpayer: ${error1}`);
          this.errorMessage = error1;
        });

    if (this.router.url.includes('edit')) {
      console.log(`Edit mode`);
      this.cardTitle = 'Edycja danych';
      this.readMode = false;
    } else {
      console.log(`Read mode`);
      this.cardTitle = 'Dane podatnika';
      this.readMode = true;
    }

    this.addressForm = this.formBuilder.group({
      voivodeship: [{value: '', disabled: this.readMode}, Validators.required],
      county: [{value: '', disabled: this.readMode}, Validators.required],
      zipCode: [{value: '', disabled: this.readMode}, [Validators.required, Validators.pattern(/^[0-9]{2}-[0-9]{3}$/)]],
      borough: [{value: '', disabled: this.readMode}, Validators.required],
      town: [{value: '', disabled: this.readMode}, Validators.required],
      street: [{value: '', disabled: this.readMode}],
      buildingNumber: [{value: '', disabled: this.readMode}, Validators.required],
      addressId: null
    });
    this.taxpayerProfileForm = this.formBuilder.group({
      NIP: [{value: '', disabled: this.readMode}, [Validators.required, Validators.pattern(/^[0-9]{10}$/)]],
      name: [{value: '', disabled: this.readMode}, Validators.required],
      email: [{value: '', disabled: this.readMode}, [Validators.required, Validators.email]],
      login: [{value: '', disabled: this.readMode}, Validators.required],
      address: this.addressForm,
      userId: null
    }, {
      validator: [ValidNIPSchema('NIP')],
    });
  }

  get addressFormFields() {
    return this.addressForm.controls;
  }

  // getter for easy access to form fields
  get form() {
    return this.taxpayerProfileForm.controls;
  }

  onGUSDataButtonClick(nip: number) {
    console.log('Downloading data from GUS for nip: ', nip);
    this.isLoadingDataFromGUS = true;
    this.gusService.getDataFromGus(nip)
      .subscribe(
        result => {
          this.form.name.setValue(result.name);
          this.addressFormFields.voivodeship.setValue(result.province);
          this.addressFormFields.county.setValue(result.county);
          this.addressFormFields.zipCode.setValue(result.zipCode);
          this.addressFormFields.borough.setValue(result.borough);
          this.addressFormFields.town.setValue(result.town);
          this.addressFormFields.street.setValue(result.street);
          this.errorMessage = '';
          this.isLoadingDataFromGUS = false;
        },
        error => {
          console.log(error.toString());
          this.errorMessage = error;
          this.isLoadingDataFromGUS = false;
        });
  }

  onSubmit(form: NgForm) {
    console.log('Submitting edit taxpayer himself profile form');
    if (this.taxpayerProfileForm.invalid) {
      console.log('Invalid data in edit taxpayer himself form');
      return;
    }
    this.taxpayerService.update(this.taxpayer.userId, form).subscribe(result => {
        this.router.navigate([`/user/taxpayer/taxpayer-details/${this.taxpayer.userId}`]);
        this.errorMessage = '';
        this.snackBar.open(`Pomyślnie zaktualizowano dane użytkownika o loginie: ${result['login']}`, 'Zamknij', {duration: 10000});
      },
      error1 => {
        this.snackBar.open(`Wystąpił błąd podczas aktualizowania danych użytkownika: ${error1}`, 'Zamknij', {duration: 10000});
      });
  }

  onEditButtonClick() {
    console.log(`Edit button click for taxpayer with id: ${this.taxpayer.userId}`);
    this.router.navigate([`/user/taxpayer/taxpayer-edit/${this.taxpayer.userId}`]);
  }

  onChangePasswordButtonClick() {
    console.log(`Change password click for taxpayer with id: ${this.taxpayer.userId}`);
    this.router.navigate([`/user/password-change/${this.taxpayer.userId}`]);
  }

  onResetPasswordButtonClick() {
    console.log(`Reset password click for taxpayer with id: ${this.taxpayer.userId}`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Resetowanie hasła użytkownika',
        message: `Hasło użytkownika o loginie: ${this.taxpayer.login} zostanie zresetowane. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.resetPassword(this.taxpayer.userId);
      }
    });
  }

  private fillTaxpayerForm() {
    this.form.login.setValue(this.taxpayer.login);
    this.form.email.setValue(this.taxpayer.email);
    this.form.NIP.setValue(this.taxpayer.nip);
    this.form.name.setValue(this.taxpayer.name);
    this.form.userId.setValue(this.taxpayer.userId);
    this.addressFormFields.voivodeship.setValue(this.taxpayer.address.voivodeship);
    this.addressFormFields.county.setValue(this.taxpayer.address.county);
    this.addressFormFields.zipCode.setValue(this.taxpayer.address.zipCode);
    this.addressFormFields.borough.setValue(this.taxpayer.address.borough);
    this.addressFormFields.town.setValue(this.taxpayer.address.town);
    this.addressFormFields.street.setValue(this.taxpayer.address.street);
    this.addressFormFields.buildingNumber.setValue(this.taxpayer.address.buildingNumber);
    this.addressFormFields.addressId.setValue(this.taxpayer.address.addressId);
  }

  undoClick() {
    console.log('Undo click');
    this.router.navigate([`/user/taxpayer/taxpayer-details/${this.taxpayer.userId}`]);
  }

  private resetPassword(userId: number) {
    this.userAccountService.resetPassword(userId).subscribe(result => {
        this.snackBar.open(`Pomyślnie zresetowano hasło dla użytkownika o id: ${result.userId} i loginie: ${result.login}.
       \n Nowe hasło: ${result.newPassword}`, 'Zamknij');
      },
      error1 => {
        console.log(`Error by reset password for user with id: ${userId}: ${error1}`);
        this.snackBar.open('Resetowanie hasła nie powiodło się', 'Zamknij', {duration: 10000});
      });
  }
}
