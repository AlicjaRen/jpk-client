import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TaxpayerDetailsComponent} from './taxpayer-details.component';

describe('TaxpayerDetailsComponent', () => {
  let component: TaxpayerDetailsComponent;
  let fixture: ComponentFixture<TaxpayerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxpayerDetailsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxpayerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
