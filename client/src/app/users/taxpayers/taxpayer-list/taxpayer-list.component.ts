import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {TaxpayersService} from '../services/taxpayers.service';
import {ConfirmationDialogComponent} from '../../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';
import {Taxpayer} from '../models/taxpayer.model';

@Component({
  selector: 'app-taxpayer-list',
  templateUrl: './taxpayer-list.component.html',
  styleUrls: ['./taxpayer-list.component.scss']
})
export class TaxpayerListComponent implements OnInit {

  componentTitle = 'Lista podatników zarejestrowanych w systemie';
  taxpayers: Taxpayer[];
  data: MatTableDataSource<Taxpayer>;
  displayedColumns: string[] = ['no', 'userId', 'login', 'name', 'nip', 'email', 'address', 'editButton', 'deleteButton'];
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(private router: Router,
              private taxpayerService: TaxpayersService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.refreshTaxpayersList();
  }

  refreshTaxpayersList() {
    this.taxpayerService.getAll().subscribe(result => {
      this.taxpayers = result;
      this.data = new MatTableDataSource<Taxpayer>(this.taxpayers);
      this.data.paginator = this.paginator;
      this.data.sort = this.sort;
    }, error1 => {
      console.log(`Error by getting all taxpayers: ${error1}`);
      this.snackBar.open('Wystąpił błąd podczas pobierania listy podatników', 'Zamknij', {duration: 10000});
    });
  }

  onRowClick(taxpayer: Taxpayer) {
    console.log(`Click on taxpayer with id: ${taxpayer.userId}`);
    this.router.navigate([`/user/taxpayer/taxpayer-details/${taxpayer.userId}`]);
  }

  onEditClick(taxpayer: Taxpayer) {
    console.log(`Edit click on taxpayer with id: ${taxpayer.userId}`);
    this.router.navigate([`/user/taxpayer/taxpayer-edit/${taxpayer.userId}`]);
  }

  onDeleteClick(taxpayer: Taxpayer) {
    console.log(`Delete click on taxpayer with id: ${taxpayer.userId}`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Usuwanie podatnika',
        message: `Konto podatnika o loginie: ${taxpayer.login} zostanie usunięte. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.deleteTaxpayer(taxpayer);
      }
    });
  }

  deleteTaxpayer(taxpayer: Taxpayer) {
    this.taxpayerService.delete(taxpayer.userId).subscribe(() => {
        this.snackBar.open(`Pomyślnie usunięto podatnika o loginie ${taxpayer.login}`, 'Zamknij', {duration: 10000});
        this.refreshTaxpayersList();
      },
      error1 => {
        console.log(`Error by deleting taxpayer with id: ${taxpayer.userId}: ${error1}`);
        this.snackBar.open(`Usuwanie podatnika o loginie: ${taxpayer.login} nie powiodło się`, 'Zamknij', {duration: 10000});
      });
  }
}
