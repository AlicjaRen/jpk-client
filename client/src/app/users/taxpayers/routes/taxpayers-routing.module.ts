import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TaxpayerAddComponent} from '../taxpayer-add/taxpayer-add.component';
import {TaxpayerDetailsComponent} from '../taxpayer-details/taxpayer-details.component';
import {TaxpayerListComponent} from '../taxpayer-list/taxpayer-list.component';
import {AdminRoleGuard} from '../../../security/guards/admin-role.guard';
import {TaxpayerProfileGuard} from '../../../security/guards/taxpayer-profile.guard';

const routes: Routes = [
  {path: 'taxpayer-add', component: TaxpayerAddComponent},
  {path: 'taxpayer-edit/:taxpayerId', component: TaxpayerDetailsComponent, canActivate: [TaxpayerProfileGuard]},
  {path: 'taxpayer-details/:taxpayerId', component: TaxpayerDetailsComponent, canActivate: [TaxpayerProfileGuard]},
  {path: '**', component: TaxpayerListComponent, canActivate: [AdminRoleGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaxpayersRoutingModule {
}
