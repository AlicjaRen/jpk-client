import {TestBed} from '@angular/core/testing';

import {TaxpayersService} from './taxpayers.service';

describe('TaxpayersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaxpayersService = TestBed.get(TaxpayersService);
    expect(service).toBeTruthy();
  });
});
