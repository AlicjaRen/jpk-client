import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {Taxpayer} from '../models/taxpayer.model';
import {tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = `${environment.API_URL}/user/taxpayer`;

@Injectable({
  providedIn: 'root'
})
export class TaxpayersService {

  constructor(private http: HttpClient) {
  }

  create(taxpayer: any): Observable<Taxpayer> {
    const url = `${apiUrl}/user`;
    return this.http.post<Taxpayer>(url, taxpayer, httpOptions).pipe(
      tap((createdTaxpayer: Taxpayer) => console.log(`created taxpayer with id= ${createdTaxpayer.userId}`)));
  }

  read(id: number): Observable<Taxpayer> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Taxpayer>(url).pipe(
      tap(() => console.log(`fetched taxpayer with id=${id}`)));
  }

  getAll(): Observable<Taxpayer[]> {
    return this.http.get<Taxpayer[]>(apiUrl).pipe(
      tap(() => console.log('fetched taxpayers')));
  }

  update(id, taxpayer): Observable<Taxpayer> {
    const url = `${apiUrl}/${id}`;
    return this.http.put<Taxpayer>(url, taxpayer).pipe(
      tap((updatedTaxpayer: Taxpayer) => console.log(`updated taxpayer with id=${updatedTaxpayer.userId}`)));
  }

  delete(id: number): Observable<Taxpayer> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Taxpayer>(url).pipe(
      tap(() => console.log(`deleted taxpayer with id=${id}`)));
  }
}
