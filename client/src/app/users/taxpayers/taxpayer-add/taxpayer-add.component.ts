import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TaxpayersService} from '../services/taxpayers.service';
import {MustMatch} from '../../shared/validators/must-match.validator';
import {UserValidationMessagesService} from '../../shared/validators/user-validation-messages.service';
import {MatSnackBar} from '@angular/material';
import {GusService} from '../../../gus/services/gus.service';
import {ValidNIPSchema} from '../../shared/validators/valid-nip-schema.validator';

@Component({
  selector: 'app-taxpayer-add',
  templateUrl: './taxpayer-add.component.html',
  styleUrls: ['./taxpayer-add.component.scss']
})
export class TaxpayerAddComponent implements OnInit {
  registerByUserForm: FormGroup;
  addressForm: FormGroup;
  passwordHide = true;
  confirmationHide = true;
  isLoadingDataFromGUS = false;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private taxpayerService: TaxpayersService,
              private gusService: GusService,
              public messages: UserValidationMessagesService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.addressForm = this.formBuilder.group({
      voivodeship: ['', Validators.required],
      county: ['', Validators.required],
      zipCode: ['', [Validators.required, Validators.pattern(/^[0-9]{2}-[0-9]{3}$/)]],
      borough: ['', Validators.required],
      town: ['', Validators.required],
      street: [''],
      buildingNumber: ['', Validators.required]
    });
    this.registerByUserForm = this.formBuilder.group({
      NIP: ['', [Validators.required, Validators.pattern(/^[0-9]{10}$/)]],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      login: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8),
        Validators.pattern('^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!*(){}\\[\\]\\-_|<>?])(?=\\S+$).{8,}$')]],
      passwordConfirmation: ['', Validators.required],
      address: this.addressForm,
    }, {
      validator: [ValidNIPSchema('NIP'), MustMatch('password', 'passwordConfirmation')],
    });
  }

  get addressFormFields() {
    return this.addressForm.controls;
  }

  // getter for easy access to form fields
  get form() {
    return this.registerByUserForm.controls;
  }

  onSubmit(form: NgForm) {
    console.log('Submitting register taxpayer himself form');
    if (this.registerByUserForm.invalid) {
      console.log('Invalid data in register taxpayer himself form');
      return;
    }
    this.taxpayerService.create(form).subscribe(result => {
        this.router.navigate(['/user/login']);
        this.errorMessage = '';
        this.snackBar.open(`Pomyślnie zarejestrowano nowego użytkownika o loginie: ${result['login']}`, 'Zamknij', {duration: 10000});
      },
      error => {
        this.errorMessage = error;
      });
  }

  onGUSDataButtonClick(nip: number) {
    console.log('Downloading data from GUS for nip: ', nip);
    this.isLoadingDataFromGUS = true;
    this.gusService.getDataFromGus(nip)
      .subscribe(
        result => {
          this.form.name.setValue(result.name);
          this.addressFormFields.voivodeship.setValue(result.province);
          this.addressFormFields.county.setValue(result.county);
          this.addressFormFields.zipCode.setValue(result.zipCode);
          this.addressFormFields.borough.setValue(result.borough);
          this.addressFormFields.town.setValue(result.town);
          this.addressFormFields.street.setValue(result.street);
          this.errorMessage = '';
          this.isLoadingDataFromGUS = false;
        },
        error => {
          console.log(error.toString());
          this.errorMessage = error;
          this.isLoadingDataFromGUS = false;
        });
  }
}
