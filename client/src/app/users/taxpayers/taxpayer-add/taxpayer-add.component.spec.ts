import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TaxpayerAddComponent} from './taxpayer-add.component';

describe('TaxpayerAddComponent', () => {
  let component: TaxpayerAddComponent;
  let fixture: ComponentFixture<TaxpayerAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxpayerAddComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxpayerAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
