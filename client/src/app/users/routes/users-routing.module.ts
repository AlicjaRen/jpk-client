import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from '../shared/login/login.component';
import {PasswordChangeComponent} from '../shared/password-change/password-change.component';
import {UserProfileGuard} from '../../security/guards/user-profile.guard';

const routes: Routes = [
  {path: 'login', component: LoginComponent},

  {path: 'password-change/:userId', component: PasswordChangeComponent, canActivate: [UserProfileGuard]},

  {path: 'taxpayer', loadChildren: '../taxpayers/taxpayers.module#TaxpayersModule'},

  {path: 'admin', loadChildren: '../admins/admins.module#AdminsModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {
}
