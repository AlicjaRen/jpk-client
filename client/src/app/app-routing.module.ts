import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './security/guards/auth.guard';

const routes: Routes = [
  {path: 'home', loadChildren: './home/home.module#HomeModule', canActivate: [AuthGuard]},
  {path: 'user', loadChildren: './users/users.module#UsersModule'},
  {path: 'record', loadChildren: './records/records.module#RecordsModule'},
  {path: 'invoice', loadChildren: './invoices/invoices.module#InvoicesModule'},
  {path: 'audit-file', loadChildren: './audit-files/audit-files.module#AuditFilesModule'},
  // otherwise redirect to login page
  {path: '**', redirectTo: 'user/login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
