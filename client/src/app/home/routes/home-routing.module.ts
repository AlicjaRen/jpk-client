import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminHomeComponent} from '../admin-home/admin-home.component';
import {TaxpayerHomeComponent} from '../taxpayer-home/taxpayer-home.component';
import {AdminRoleGuard} from '../../security/guards/admin-role.guard';
import {TaxpayerRoleGuard} from '../../security/guards/taxpayer-role-guard';

const routes: Routes = [
  {
    path: 'admin',
    component: AdminHomeComponent,
    canActivate: [AdminRoleGuard]
  },
  {
    path: 'taxpayer',
    component: TaxpayerHomeComponent,
    canActivate: [TaxpayerRoleGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
