import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-taxpayer-home',
  templateUrl: './taxpayer-home.component.html',
  styleUrls: ['./taxpayer-home.component.scss']
})
export class TaxpayerHomeComponent implements OnInit {

  componentTitle = 'Witaj Podatniku! Pamiętaj o obowiązku złożenia JPK_VAT do 25 dnia miesiąca!';

  constructor(
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  onNewSaleInvoiceClick() {
    this.router.navigate(['/invoice/sale/sale-add']);
  }

  onNewPurchaseInvoiceClick() {
    this.router.navigate(['/invoice/purchase/purchase-add']);
  }

  onGenerateSAFTClick() {
    this.router.navigate(['/audit-file']);
  }

  onDownloadSAFTClick() {
    this.router.navigate(['audit-file']);
  }

  onSaleRecordClick() {
    this.router.navigate(['/record/sale']);
  }

  onPurchaseRecordClick() {
    this.router.navigate(['/record/purchase']);
  }
}
