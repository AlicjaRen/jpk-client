import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TaxpayerHomeComponent} from './taxpayer-home.component';

describe('TaxpayerHomeComponent', () => {
  let component: TaxpayerHomeComponent;
  let fixture: ComponentFixture<TaxpayerHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TaxpayerHomeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxpayerHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
