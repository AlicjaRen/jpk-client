import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {

  componentTitle = 'Witaj Administratorze!';

  constructor(
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  onTaxpayersListClick() {
    this.router.navigate(['/user/taxpayer']);
  }

  onEditTaxpayerClick() {
    this.router.navigate(['/user/taxpayer']);
  }

  onAdminListClick() {
    this.router.navigate(['/user/admin']);
  }

  onCreateNewAdminClick() {
    this.router.navigate(['/user/admin/admin-add']);
  }
}
