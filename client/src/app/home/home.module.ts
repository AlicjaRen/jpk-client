import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CommonTemplatesModule} from '../common-templates/common-templates.module';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {HomeRoutingModule} from './routes/home-routing.module';
import {AdminHomeComponent} from './admin-home/admin-home.component';
import {TaxpayerHomeComponent} from './taxpayer-home/taxpayer-home.component';

@NgModule({
  declarations: [
    AdminHomeComponent,
    TaxpayerHomeComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    CommonTemplatesModule,
    HomeRoutingModule
  ]
})
export class HomeModule {
}
