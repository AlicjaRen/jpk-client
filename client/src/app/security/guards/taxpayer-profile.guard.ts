import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {UserRole} from '../../users/shared/models/user-role.enum';

@Injectable({
  providedIn: 'root'
})
export class TaxpayerProfileGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.currentUserValue;

    // user with ADMIN role has access for all users profiles
    if (currentUser && UserRole.ADMIN === UserRole [currentUser.userRole]) {
      return true;
    }

    console.log(`param map[taxpayerId]: ${route.paramMap.get('taxpayerId')}`);

    // taxpayer has access only for own profile
    if (currentUser && UserRole.TAXPAYER === UserRole [currentUser.userRole]
      && route.paramMap.get('taxpayerId') === currentUser.userId.toString()) {
      return true;
    }

    // not logged or not admin or not this taxpayer, redirect to login page and remember url from this request (for further redirect)
    this.router.navigate(['/user/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}
