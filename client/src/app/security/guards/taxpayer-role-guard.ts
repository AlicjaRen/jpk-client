import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {UserRole} from '../../users/shared/models/user-role.enum';

@Injectable({
  providedIn: 'root'
})
export class TaxpayerRoleGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser && UserRole.TAXPAYER === UserRole [currentUser.userRole]) { // user with TAXPAYER role
      return true;
    }

    // not logged or not taxpayer, redirect to login page and remember url from this request (for further redirect)
    this.router.navigate(['/user/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }
}
