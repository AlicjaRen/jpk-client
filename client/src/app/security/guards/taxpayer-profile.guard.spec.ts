import {inject, TestBed} from '@angular/core/testing';

import {TaxpayerProfileGuard} from './taxpayer-profile.guard';

describe('TaxpayerProfileGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaxpayerProfileGuard]
    });
  });

  it('should ...', inject([TaxpayerProfileGuard], (guard: TaxpayerProfileGuard) => {
    expect(guard).toBeTruthy();
  }));
});
