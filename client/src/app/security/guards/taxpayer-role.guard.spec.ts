import {inject, TestBed} from '@angular/core/testing';

import {TaxpayerRoleGuard} from './taxpayer-role-guard';

describe('TaxpayerRoleGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaxpayerRoleGuard]
    });
  });

  it('should ...', inject([TaxpayerRoleGuard], (guard: TaxpayerRoleGuard) => {
    expect(guard).toBeTruthy();
  }));
});
