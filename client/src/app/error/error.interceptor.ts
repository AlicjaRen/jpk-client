import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {AuthenticationService} from '../security/services/authentication.service';
import {Status400MessagesService} from './services/status400-messages.service';

@Injectable({providedIn: 'root'})
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private  authenticationService: AuthenticationService,
              private  status400MessagesService: Status400MessagesService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(catchError(err => {
      if (err.status === 401 && this.authenticationService.currentUserValue) {
        this.authenticationService.logout();
        location.reload(true);
        console.log(`${err.message}`);
        return throwError('Zbyt długi czas od ostatniego logowania. Prosimy o ponowne zalogowanie');
      }

      if (err.status === 401) {
        console.log(`error 401: ${err.error.exception}`);
        return throwError('Błędne dane logowania');
      }

      if (err.status === 400) {
        console.log(`error 400: ${err.error.exception}`);
        const errorMessage = this.status400MessagesService.getMessage(err.error.exception);
        return throwError(errorMessage);
      }

      if (err.status === 500) {
        console.log('Internal server error');
        return throwError('Przepraszamy, ma miejsce awaria serwisu. Prosimy spróbować ponownie');
      }

      if (err.status === 0) {
        console.log('Backend does not respond');
        return throwError('Przepraszamy, przerwa techniczna w działaniu serwisu. Prosimy spróbować ponownie');
      }
      console.log(`status error: ${err.status}`);
      // 'Przepraszamy, przerwa techniczna w działaniu serwisu. Prosimy spróbować ponownie'
      // todo add 501 status (internal server error, 403 forbidden)
      const error = err.error.message || err.statusText;
      return throwError(error);
    }));
  }
}
