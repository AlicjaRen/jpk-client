import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Status400MessagesService {

  constructor() {
  }

  private messages = {
    'class jpk.exception.exceptions.NIPNotFoundException': {message: 'NIP nie został znaleziony w GUS'},
    'class jpk.exception.exceptions.NotUniqueLoginException': {message: 'Taki login już istnieje w systemie'}
  };
  private defaultMessage = 'Błędne dane';

  getMessage(exceptionClass: string) {
    if (this.messages.hasOwnProperty(exceptionClass)) {
      return this.messages[exceptionClass].message;
    } else {
      return this.defaultMessage;
    }
  }
}
