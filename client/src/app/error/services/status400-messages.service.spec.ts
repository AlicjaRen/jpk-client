import {TestBed} from '@angular/core/testing';

import {Status400MessagesService} from './status400-messages.service';

describe('Status400MessagesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Status400MessagesService = TestBed.get(Status400MessagesService);
    expect(service).toBeTruthy();
  });
});
