import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: 'purchase', loadChildren: '../purchase-invoices/purchase-invoices.module#PurchaseInvoicesModule'},

  {path: 'sale', loadChildren: '../sale-invoices/sale-invoices.module#SaleInvoicesModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule {
}
