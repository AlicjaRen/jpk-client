import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {InvoiceElement} from '../models/invoice-element';
import {TaxRate} from '../models/tax-rate.enum';
import {FormBuilder, FormGroup, NgForm} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-invoice-element-add',
  templateUrl: './invoice-element.component.html',
  styleUrls: ['./invoice-element.component.scss']
})
export class InvoiceElementComponent implements OnInit {

  elementForm: FormGroup;
  taxRate = TaxRate;

  // error messages
  invalidAmountMsg = 'Pole wymagane w postaci: 0.00';
  requiredMsg = 'Pole wymagane';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private dialogRef: MatDialogRef<InvoiceElementComponent>,
    @Inject(MAT_DIALOG_DATA) private data) {
  }

  ngOnInit() {
    this.elementForm = this.formBuilder.group({
      name: this.data ? this.data.name : [''],
      net: this.data ? this.data.net : [''],
      taxRate: this.data ? this.data.taxRate : [''],
      vat: this.data ? this.data.vat : [''],
      gross: this.data ? this.data.gross : [''],
      invoiceElementId: this.data ? this.data.invoiceElementId : null
    });
  }

  get form() {
    return this.elementForm.controls;
  }

  onChangeTaxRate() {
    this.countSuggestedVat();
    this.countSuggestedGross();
  }

  onChangeNet() {
    this.countSuggestedVat();
    this.countSuggestedGross();
  }

  onChangeVat() {
    this.countSuggestedGross();
  }

  countSuggestedVat() {
    if (this.elementForm.value.net !== undefined && this.elementForm.value.taxRate !== '') {
      const selectedRate = parseInt(TaxRate[this.elementForm.value.taxRate], 10);
      const suggestedVat: number = Math.round(this.elementForm.value.net * selectedRate * 0.01 * 100) / 100;
      this.form.vat.setValue(suggestedVat);
    }
  }

  countSuggestedGross() {
    if (this.elementForm.value.net !== undefined && this.elementForm.value.vat !== '') {
      console.log(`new vat: ${this.elementForm.value.vat}, net: ${this.elementForm.value.net}`);
      const suggestedGross: number = Math.round(((parseFloat(this.elementForm.value.net)
        + parseFloat(this.elementForm.value.vat)) * 100)) / 100;
      console.log(`suggested gross: ${suggestedGross}`);
      this.form.gross.setValue(suggestedGross);
    }
  }

  submit(elementForm: NgForm) {
    const invoiceElement = <InvoiceElement>elementForm.value;
    this.dialogRef.close(invoiceElement);
    console.log(`close dialog with values: name: ${invoiceElement.name}`);
  }
}
