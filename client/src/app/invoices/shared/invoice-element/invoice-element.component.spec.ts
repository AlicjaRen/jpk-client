import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {InvoiceElementComponent} from './invoice-element.component';

describe('InvoiceElementComponent', () => {
  let component: InvoiceElementComponent;
  let fixture: ComponentFixture<InvoiceElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InvoiceElementComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
