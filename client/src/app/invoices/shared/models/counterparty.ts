import {Address} from '../../../users/taxpayers/models/address.model';

export class Counterparty {
  counterpartyId: number;
  name: string;
  nip: number;
  address: Address;
}
