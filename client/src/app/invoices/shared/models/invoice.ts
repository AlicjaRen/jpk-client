import {Counterparty} from './counterparty';
import {InvoiceElement} from './invoice-element';

export class Invoice {
  invoiceId: number;
  number: string;
  dateOfIssue: Date;
  netHigh: number;
  netMedium: number;
  netLow: number;
  vatHigh: number;
  vatMedium: number;
  vatLow: number;
  gross: number;
  counterparty: Counterparty;
  invoiceElements: Set<InvoiceElement>;
  recordId: number;
}
