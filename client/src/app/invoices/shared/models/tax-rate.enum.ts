export enum TaxRate {
  HIGH = 23,
  MEDIUM = 8,
  LOW = 5
}

export namespace TaxRate {
  export function values() {
    return Object.keys(TaxRate)
      .map(key => TaxRate[key])
      .filter(value => typeof value === 'number') as string[];
  }

  export function findKeyForValue(value: number) {
    let keys;
    console.log(`value: ${value}`);
    keys = Object.keys(TaxRate).filter(k => !isNaN(Number(k)));
    for (const k of keys) {
      console.log(`k= ${k} value= ${value}`);
      console.log(`taxRate[k]: ${TaxRate[k]}`);
      if (k === value) {
        console.log(`Found: ${TaxRate[k]}`);
        return TaxRate[k];
      }
    }
  }
}
