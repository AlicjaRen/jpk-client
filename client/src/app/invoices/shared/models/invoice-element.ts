import {TaxRate} from './tax-rate.enum';

export class InvoiceElement {
  invoiceElementId: number;
  name: string;
  net: number;
  vat: number;
  gross: number;
  taxRate: TaxRate;
}
