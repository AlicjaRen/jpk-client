import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../angular-material/angular-material.module';
import {CommonTemplatesModule} from '../../common-templates/common-templates.module';
import {InvoicesModule} from '../invoices.module';
import {InvoiceElementComponent} from './invoice-element/invoice-element.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    CommonTemplatesModule,
    InvoicesModule
  ],
  declarations: [
    InvoiceElementComponent
  ]
})
export class SharedModule {
}
