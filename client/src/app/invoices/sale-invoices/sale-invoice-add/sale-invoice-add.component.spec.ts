import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SaleInvoiceAddComponent} from './sale-invoice-add.component';

describe('SaleInvoiceAddComponent', () => {
  let component: SaleInvoiceAddComponent;
  let fixture: ComponentFixture<SaleInvoiceAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SaleInvoiceAddComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleInvoiceAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
