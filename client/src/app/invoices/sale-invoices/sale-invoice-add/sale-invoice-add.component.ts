import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {SaleInvoiceService} from '../services/sale-invoice.service';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDialog, MatDialogRef, MatSnackBar, MatTableDataSource} from '@angular/material';
import {GusService} from '../../../gus/services/gus.service';
import {TaxRate} from '../../shared/models/tax-rate.enum';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {Location} from '@angular/common';
import {InvoiceElement} from '../../shared/models/invoice-element';
import {InvoiceElementComponent} from '../../shared/invoice-element/invoice-element.component';
import {ConfirmationDialogComponent} from '../../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';
import {ReckoningService} from '../../../monthly-reckoning/services/reckoning.service';
import {ReckoningItem} from '../../../monthly-reckoning/models/reckoning-item.model';
import {InvoiceProvider} from '../../shared/providers/invoice-provider';
import {SaleInvoice} from '../model/sale-invoice';
import {ValidNIPSchema} from '../../../users/shared/validators/valid-nip-schema.validator';

@Component({
  selector: 'app-sale-invoice-add',
  templateUrl: './sale-invoice-add.component.html',
  styleUrls: ['./sale-invoice-add.component.scss'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'ja-JP'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ],
})
export class SaleInvoiceAddComponent implements OnInit {

  componentTitle: string;
  cardTitle: string;
  saleInvoiceForm: FormGroup;
  addressForm: FormGroup;
  counterpartyForm: FormGroup;
  errorMessage: string;
  taxRate = TaxRate;
  maxDate = new Date();
  displayedColumns: string[] = ['name', 'net', 'taxRate', 'vat', 'gross', 'editButton', 'deleteButton'];
  elements: Array<InvoiceElement> = new Array<InvoiceElement>();
  data: MatTableDataSource<InvoiceElement>;
  invoiceElementDialog: MatDialogRef<InvoiceElementComponent>;
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;
  isLoadingDataFromGUS = false;

  amountPattern = /^[+-]?[0-9]+([.]([0-9]){1,2})?$/;
  // error messages
  invalidAmountMsg = 'Pole wymagane w postaci: 0.00';
  requiredMsg = 'Pole wymagane';
  invalidNIPMsg = 'Błędny NIP: wymagane 10 cyfr';
  invalidZipCodeMsg = 'Pole wymagane w postaci: NN-NNN';

  saleInvoice: SaleInvoice;
  editMode = false;
  readMode = false;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private saleInvoiceService: SaleInvoiceService,
              private reckoningService: ReckoningService,
              private gusService: GusService,
              private snackBar: MatSnackBar,
              private adapter: DateAdapter<any>,
              private dialog: MatDialog,
              private invoiceProvider: InvoiceProvider,
              private location: Location) {
  }

  ngOnInit() {
    console.log(`Inject invoice from provider: ${JSON.stringify(this.invoiceProvider.storage)}`);
    console.log(`current URL: ${this.router.url}`);
    if (this.router.url.includes('edit') && this.invoiceProvider.storage) {
      this.saleInvoice = <SaleInvoice>JSON.parse(this.invoiceProvider.storage);
      this.elements = Array.from(this.saleInvoice.invoiceElements);
      this.editMode = true;
      this.componentTitle = 'Faktura sprzedaży';
      this.cardTitle = 'Edycja faktury sprzedaży';
      console.log(`Sale invoice in edit mode in providers with id: ${this.saleInvoice.invoiceId}`);

    } else if (this.router.url.includes('details') && this.invoiceProvider.storage) {
      this.saleInvoice = <SaleInvoice>JSON.parse(this.invoiceProvider.storage);
      this.elements = Array.from(this.saleInvoice.invoiceElements);
      this.readMode = true;
      this.componentTitle = 'Faktura sprzedaży';
      this.cardTitle = 'Dane faktury sprzedaży';
      console.log(`Sale invoice in read mode in providers with id: ${this.saleInvoice.invoiceId}`);

    } else {
      this.componentTitle = 'Nowa faktura sprzedaży';
      this.cardTitle = 'Dodanie faktury sprzedaży';
    }

    this.adapter.setLocale('pl');
    this.addressForm = this.formBuilder.group({
      voivodeship: this.saleInvoice ? new FormControl({
        value: this.saleInvoice.counterparty.address.voivodeship,
        disabled: this.readMode
      }) : ['', Validators.required],
      county: this.saleInvoice ? new FormControl({value: this.saleInvoice.counterparty.address.county, disabled: this.readMode})
        : ['', Validators.required],
      zipCode: this.saleInvoice ? new FormControl({value: this.saleInvoice.counterparty.address.zipCode, disabled: this.readMode})
        : ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]{2}-[0-9]{3}$/)])],
      borough: this.saleInvoice ? new FormControl({value: this.saleInvoice.counterparty.address.borough, disabled: this.readMode})
        : ['', Validators.required],
      town: this.saleInvoice ? new FormControl({value: this.saleInvoice.counterparty.address.town, disabled: this.readMode})
        : ['', Validators.required],
      street: this.saleInvoice ? new FormControl({value: this.saleInvoice.counterparty.address.street, disabled: this.readMode})
        : [''],
      buildingNumber: this.saleInvoice ? new FormControl({
        value: this.saleInvoice.counterparty.address.buildingNumber,
        disabled: this.readMode
      }) : ['', Validators.required],
      addressId: this.saleInvoice ? this.saleInvoice.counterparty.address.addressId : null
    });

    this.counterpartyForm = this.formBuilder.group({
      name: this.saleInvoice ? new FormControl({value: this.saleInvoice.counterparty.name, disabled: this.readMode})
        : ['', Validators.required],
      nip: this.saleInvoice ? new FormControl({value: this.saleInvoice.counterparty.nip, disabled: this.readMode})
        : ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]{10}$/)])],
      address: this.addressForm,
      counterpartyId: this.saleInvoice ? this.saleInvoice.counterparty.counterpartyId : null
    }, {
      validator: [ValidNIPSchema('nip')],
    });

    this.saleInvoiceForm = this.formBuilder.group({
      number: this.saleInvoice ? new FormControl({value: this.saleInvoice.number, disabled: this.readMode})
        : ['', [Validators.required]],
      dateOfIssue: this.saleInvoice ? new FormControl({value: this.saleInvoice.dateOfIssue, disabled: this.readMode})
        : ['', Validators.required],
      netHigh: this.saleInvoice ? new FormControl({value: this.saleInvoice.netHigh, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      netMedium: this.saleInvoice ? new FormControl({value: this.saleInvoice.netMedium, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      netLow: this.saleInvoice ? new FormControl({value: this.saleInvoice.netLow, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      vatHigh: this.saleInvoice ? new FormControl({value: this.saleInvoice.vatHigh, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      vatMedium: this.saleInvoice ? new FormControl({value: this.saleInvoice.vatMedium, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      vatLow: this.saleInvoice ? new FormControl({value: this.saleInvoice.vatLow, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      gross: this.saleInvoice ? new FormControl({value: this.saleInvoice.gross, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      counterparty: this.counterpartyForm,
      invoiceElements: this.elements,
      invoiceId: this.saleInvoice ? this.saleInvoice.invoiceId : null
    });

    this.data = new MatTableDataSource<InvoiceElement>(Array.from(this.elements));
  }

  get addressFormFields() {
    return this.addressForm.controls;
  }

  get counterpartyFields() {
    return this.counterpartyForm.controls;
  }

  get form() {
    return this.saleInvoiceForm.controls;
  }

  openDialog(element?): void {
    this.invoiceElementDialog = this.dialog.open(InvoiceElementComponent, {
      hasBackdrop: false,
      data: {
        name: element ? element.name : ['', [Validators.required]],
        net: element ? element.net : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
        taxRate: element ? element.taxRate : ['', [Validators.required]],
        vat: element ? element.vat : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
        gross: element ? element.gross : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
        invoiceElementId: element ? element.invoiceElementId : null,
      },
      width: '450px'
    });

    this.invoiceElementDialog.afterClosed().subscribe(result => {
      console.log(`The dialog was closed with result name: ${result.name} for element with id ${result.invoiceElementId}`);
      if (result) {
        if (element) {
          const index: number = this.elements.indexOf(element);
          if (index !== -1) {
            this.elements[index] = result;
          }
        } else {
          this.elements.push(result);
        }
        console.log(`Result data: name: ${result['name']}, taxRate: ${result.taxRate}, id: ${result['invoiceElementId']}`);
        this.data = new MatTableDataSource<InvoiceElement>(this.elements);
        this.form.invoiceElements.setValue(this.elements);
        this.countSuggestedSummary();
      } else {
        console.log('Only undo');
      }
    });
  }

  deleteElement(element): void {
    console.log(`Deleting element with name ${element.name}`);
    const index: number = this.elements.indexOf(element);
    if (index !== -1) {
      this.elements.splice(index, 1);
      this.data = new MatTableDataSource<InvoiceElement>(this.elements);
      this.form.invoiceElements.setValue(this.elements);
      this.countSuggestedSummary();
    }
  }

  countSuggestedSummary() {
    let sumNetHigh = 0.0;
    let sumVatHigh = 0.0;
    let sumNetMedium = 0.0;
    let sumVatMedium = 0.0;
    let sumNetLow = 0.0;
    let sumVatLow = 0.0;
    let gross = 0.0;

    for (const element of this.elements) {
      if (parseInt(TaxRate[element.taxRate], 10) === parseInt(TaxRate.HIGH.toString(), 10)) {
        sumNetHigh += parseFloat(element.net.toString());
        sumVatHigh += element.vat;
      } else if (parseInt(TaxRate[element.taxRate], 10) === parseInt(TaxRate.MEDIUM.toString(), 10)) {
        sumNetMedium += parseFloat(element.net.toString());
        sumVatMedium += element.vat;
      } else {
        sumNetLow += parseFloat(element.net.toString());
        sumVatLow += element.vat;
      }
      gross += element.gross;
    }

    this.form.netHigh.setValue(Math.round(sumNetHigh * 100) / 100);
    this.form.vatHigh.setValue(Math.round(sumVatHigh * 100) / 100);
    this.form.netMedium.setValue(Math.round(sumNetMedium * 100) / 100);
    this.form.vatMedium.setValue(Math.round(sumVatMedium * 100) / 100);
    this.form.netLow.setValue(Math.round(sumNetLow * 100) / 100);
    this.form.vatLow.setValue(Math.round(sumVatLow * 100) / 100);
    this.form.gross.setValue(Math.round(gross * 100) / 100);
  }

  countSuggestedGross() {
    if (this.saleInvoiceForm.value.netHigh !== '' && this.saleInvoiceForm.value.vatHigh !== ''
      && this.saleInvoiceForm.value.netMedium !== '' && this.saleInvoiceForm.value.vatMedium !== ''
      && this.saleInvoiceForm.value.netLow !== '' && this.saleInvoiceForm.value.vatLow !== '') {
      const suggestedGross = parseFloat(this.saleInvoiceForm.value.netHigh) + parseFloat(this.saleInvoiceForm.value.vatHigh)
        + parseFloat(this.saleInvoiceForm.value.netMedium) + parseFloat(this.saleInvoiceForm.value.vatMedium)
        + parseFloat(this.saleInvoiceForm.value.netLow) + parseFloat(this.saleInvoiceForm.value.vatLow);
      this.form.gross.setValue(suggestedGross);
    }
  }

  onSubmit(form: NgForm) {
    console.log(`Submitting new sale invoice form with number of elements = ${form['invoiceElements'].length}`);
    if (this.saleInvoiceForm.invalid) {
      console.log('Invalid data in new sale invoice form');
      return;
    }

    if (this.saleInvoiceForReckoningWithJPK(form['dateOfIssue'])) {
      console.log('displaying dialog with confirmation for adding sale invoice');
      this.submitAfterConfirmation(form);
    } else {
      this.submitInvoiceForm(form);
    }
  }

  private submitInvoiceForm(form: NgForm) {
    if (this.editMode) {
      console.log('Send as edit');
      form['invoiceElements'] = this.elements;
      console.log(`Submitting updated sale invoice form with number of elements = ${form['invoiceElements'].length}`);
      this.saleInvoiceService.update(this.saleInvoice.invoiceId, form).subscribe(result => {
          const saleRecordId = result['recordId'];
          this.router.navigate([`/record/sale/${saleRecordId}`]);
          this.errorMessage = '';
          this.snackBar.open(`Zaktualizowano dane faktury sprzedaży o numerze: ${result['number']}`, 'Zamknij', {duration: 10000});
        },
        error => {
          this.errorMessage = error;
        });
    } else {
      this.saleInvoiceService.create(form).subscribe(result => {
          const saleRecordId = result['recordId'];
          this.router.navigate([`/record/sale/${saleRecordId}`]);
          this.errorMessage = '';
          this.snackBar.open(`Dodano fakturę sprzedaży o numerze: ${result['number']}`, 'Zamknij', {duration: 10000});
        },
        error => {
          this.errorMessage = error;
        });
    }
  }

  onGUSDataButtonClick(nip: number) {
    this.isLoadingDataFromGUS = true;
    console.log('Downloading data from GUS for nip: ', nip);
    this.gusService.getDataFromGus(nip)
      .subscribe(
        result => {
          this.counterpartyFields.name.setValue(result.name);
          this.addressFormFields.voivodeship.setValue(result.province);
          this.addressFormFields.county.setValue(result.county);
          this.addressFormFields.zipCode.setValue(result.zipCode);
          this.addressFormFields.borough.setValue(result.borough);
          this.addressFormFields.town.setValue(result.town);
          this.addressFormFields.street.setValue(result.street);
          this.errorMessage = '';
          this.isLoadingDataFromGUS = false;
        },
        error => {
          console.log(error.toString());
          this.errorMessage = error;
          this.isLoadingDataFromGUS = false;
        });
  }

  private saleInvoiceForReckoningWithJPK(date: Date): boolean {
    const reckoningForInvoiceName = this.findReckoningNameForSaleDate(date);

    this.reckoningService.getReckoningNames().subscribe(result => {
        console.log(`Get reckoning with suggested reckoning name: ${result.currentSuggestedReckoning.name}`);
        const foundReckoning: ReckoningItem = result.reckonings
          .find(value => value.name === reckoningForInvoiceName);
        return foundReckoning !== undefined ? foundReckoning.auditFileId !== null : false;
      },
      err => {
        console.log(`Error by getting reckonings: ${err}`);
        this.snackBar.open(`Wystąpił bład. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});
      });
    return;
  }

  private submitAfterConfirmation(form: NgForm) {
    console.log('Adding sale invoice for reckoning with jpk');
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Faktura sprzedaży dla istniejącego JPK',
        message: 'Faktura zostanie dodana do okresu, dla którego wygenerowano już JPK. Czy chcesz kontynuować?'
      }
    });

    this.confirmationDialog.afterClosed().subscribe(value => {
      if (value) {
        this.submitInvoiceForm(form);
      }
    });
    return;
  }

  private findReckoningNameForSaleDate(date: Date): string {
    const readableFormatDate: Date = new Date(date);
    const month = readableFormatDate.getMonth() + 1;
    const year = readableFormatDate.getFullYear();

    let reckoningName: string;
    if (month < 10) {
      reckoningName = '0' + month + '/' + year;
    } else {
      reckoningName = month + '/' + year;
    }

    console.log(`Found reckoning name: ${reckoningName} for month: ${month} and year: ${year}`);
    return reckoningName;
  }

  undoClick() {
    this.location.back();
  }
}
