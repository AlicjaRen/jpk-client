import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {SaleInvoice} from '../model/sale-invoice';
import {TaxRate} from '../../shared/models/tax-rate.enum';
import {Router} from '@angular/router';
import {InvoiceProvider} from '../../shared/providers/invoice-provider';
import {SaleInvoiceService} from '../services/sale-invoice.service';
import {ConfirmationDialogComponent} from '../../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-sale-invoice-list',
  templateUrl: './sale-invoice-list.component.html',
  styleUrls: ['./sale-invoice-list.component.scss']
})
export class SaleInvoiceListComponent implements OnInit, OnChanges {

  @Input() invoices: SaleInvoice[];

  data: MatTableDataSource<SaleInvoice>;
  displayedColumns: string[] = ['no', 'dateOfIssue', 'number', 'nip', 'name', 'netLow', 'vatLow', 'netMedium', 'vatMedium', 'netHigh',
    'vatHigh', 'gross', 'editButton', 'deleteButton'];
  taxRate = TaxRate;
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;


  constructor(
    private router: Router,
    private invoiceProvider: InvoiceProvider,
    private saleInvoiceService: SaleInvoiceService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.data = new MatTableDataSource<SaleInvoice>(this.invoices);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  onRowClick(invoice: SaleInvoice) {
    console.log(`Details for sale invoice with id: ${invoice.invoiceId} and number: ${invoice.number}`);
    this.invoiceProvider.storage = JSON.stringify(invoice);
    console.log(`In storage saved: ${JSON.stringify(this.invoiceProvider.storage)}`);
    this.router.navigate([`/invoice/sale/sale-details/${invoice.invoiceId}`]);
  }

  onEditClick(invoice: SaleInvoice) {
    console.log(`Edit sale invoice with id: ${invoice.invoiceId}`);
    this.invoiceProvider.storage = JSON.stringify(invoice);
    console.log(`In storage saved: ${JSON.stringify(this.invoiceProvider.storage)}`);
    this.router.navigate([`/invoice/sale/sale-edit/${invoice.invoiceId}`]);
  }

  onDeleteClick(invoice: SaleInvoice) {
    console.log(`Delete sale invoice with id: ${invoice.invoiceId}`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Usuwanie faktury',
        message: `Faktura u numerze: ${invoice.number} zostanie usunięta. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.deleteInvoice(invoice);
      }
    });
  }

  deleteInvoice(invoice: SaleInvoice) {
    this.saleInvoiceService.delete(invoice.invoiceId).subscribe((resposne) => {
        console.log(`Sale invoice deleted, response: ${resposne}`);
        this.router.navigate([`/record/${invoice.recordId}`]);
        this.snackBar.open(`Usunięto fakturę sprzedaży o numerze: ${invoice.number}`, 'Zamknij', {duration: 10000});
      },
      error => {
        console.log(`Error by deleting invoice: ${error}`);
        this.snackBar.open(`Błąd podczas usuwania faktury sprzedaży o numerze: ${invoice.number}. Prosimy spróbwać ponownie.`,
          'Zamknij', {duration: 10000});
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.data = new MatTableDataSource<SaleInvoice>(this.invoices);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }
}
