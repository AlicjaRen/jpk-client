import {TestBed} from '@angular/core/testing';

import {SaleInvoiceService} from './sale-invoice.service';

describe('SaleInvoiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SaleInvoiceService = TestBed.get(SaleInvoiceService);
    expect(service).toBeTruthy();
  });
});
