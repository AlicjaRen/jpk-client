import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {SaleInvoice} from '../model/sale-invoice';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = `${environment.API_URL}/invoice/sale`;

@Injectable({
  providedIn: 'root'
})
export class SaleInvoiceService {

  constructor(private http: HttpClient) {
  }

  create(saleInvoice: any): Observable<SaleInvoice> {
    const url = `${apiUrl}`;
    return this.http.post<SaleInvoice>(url, saleInvoice, httpOptions).pipe(
      tap((createdSaleInvoice: SaleInvoice) =>
        console.log(`created sale invoice with id= ${createdSaleInvoice.invoiceId}`)));
  }

  update(id, saleInvoice): Observable<SaleInvoice> {
    const url = `${apiUrl}/${id}`;
    return this.http.put<SaleInvoice>(url, saleInvoice, httpOptions).pipe(
      tap((updatedSaleInvoice: SaleInvoice) => console.log(`updated saleInvoice with id=${updatedSaleInvoice.invoiceId}`)));
  }

  delete(id: number) {
    console.log(`Deleting sale invoice with id=${id}`);
    const url = `${apiUrl}/${id}`;
    return this.http.delete(url);
  }
}
