import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SaleInvoiceAddComponent} from '../sale-invoice-add/sale-invoice-add.component';
import {TaxpayerRoleGuard} from '../../../security/guards/taxpayer-role-guard';
import {SaleInvoiceListComponent} from '../sale-invoice-list/sale-invoice-list.component';

const routes: Routes = [
  {path: 'sale-add', component: SaleInvoiceAddComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'sale-edit/:addressId', component: SaleInvoiceAddComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'sale-details/:addressId', component: SaleInvoiceAddComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'sale-list', component: SaleInvoiceListComponent, canActivate: [TaxpayerRoleGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaleInvoicesRoutingModule {
}
