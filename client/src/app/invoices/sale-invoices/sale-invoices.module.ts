import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SaleInvoiceAddComponent} from './sale-invoice-add/sale-invoice-add.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../angular-material/angular-material.module';
import {CommonTemplatesModule} from '../../common-templates/common-templates.module';
import {SaleInvoicesRoutingModule} from './routes/sale-invoices-routing.module';
import {SaleInvoiceListComponent} from './sale-invoice-list/sale-invoice-list.component';
import {InvoicesModule} from '../invoices.module';
import {InvoiceElementComponent} from '../shared/invoice-element/invoice-element.component';
import {SharedModule} from '../shared/shared.module';
import {ConfirmationDialogComponent} from '../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    SaleInvoicesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    CommonTemplatesModule,
    InvoicesModule,
    SharedModule
  ],
  declarations: [
    SaleInvoiceAddComponent,
    SaleInvoiceListComponent
  ],
  exports: [
    SaleInvoiceListComponent
  ],
  entryComponents: [
    InvoiceElementComponent,
    ConfirmationDialogComponent
  ]
})
export class SaleInvoicesModule {
}
