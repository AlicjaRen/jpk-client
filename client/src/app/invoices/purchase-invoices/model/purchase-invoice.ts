import {Invoice} from '../../shared/models/invoice';

export class PurchaseInvoice extends Invoice {
  reckoningName: string;
  deducted: boolean;
  fixedAsset: boolean;
}
