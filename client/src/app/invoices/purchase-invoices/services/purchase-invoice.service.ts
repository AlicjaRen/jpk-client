import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {PurchaseInvoice} from '../model/purchase-invoice';
import {environment} from '../../../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = `${environment.API_URL}/invoice/purchase`;

@Injectable({
  providedIn: 'root'
})
export class PurchaseInvoiceService {

  constructor(private http: HttpClient) {
  }

  create(purchaseInvoice: any): Observable<PurchaseInvoice> {
    const url = `${apiUrl}`;
    return this.http.post<PurchaseInvoice>(url, purchaseInvoice, httpOptions).pipe(
      tap((createdPurchaseInvoice: PurchaseInvoice) =>
        console.log(`created purchase invoice with id= ${createdPurchaseInvoice.invoiceId}`)));
  }

  update(id, purchaseInvoice): Observable<PurchaseInvoice> {
    const url = `${apiUrl}/${id}`;
    return this.http.put<PurchaseInvoice>(url, purchaseInvoice, httpOptions).pipe(
      tap((updatedPurchaseInvoice: PurchaseInvoice) => console.log(
        `updated purchase invoice with id=${updatedPurchaseInvoice.invoiceId}`)));
  }

  delete(id: number) {
    console.log(`Deleting purchase invoice with id=${id}`);
    const url = `${apiUrl}/${id}`;
    return this.http.delete(url);
  }
}
