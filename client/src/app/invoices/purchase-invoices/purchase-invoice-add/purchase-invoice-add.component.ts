import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {TaxRate} from '../../shared/models/tax-rate.enum';
import {InvoiceElement} from '../../shared/models/invoice-element';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, MatDialog, MatDialogRef, MatSnackBar, MatTableDataSource} from '@angular/material';
import {InvoiceElementComponent} from '../../shared/invoice-element/invoice-element.component';
import {ActivatedRoute, Router} from '@angular/router';
import {GusService} from '../../../gus/services/gus.service';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {Location} from '@angular/common';
import {PurchaseInvoiceService} from '../services/purchase-invoice.service';
import {ReckoningService} from '../../../monthly-reckoning/services/reckoning.service';
import {ReckoningItem} from '../../../monthly-reckoning/models/reckoning-item.model';
import {ConfirmationDialogComponent} from '../../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';
import {PurchaseInvoice} from '../model/purchase-invoice';
import {InvoiceProvider} from '../../shared/providers/invoice-provider';
import {ValidNIPSchema} from '../../../users/shared/validators/valid-nip-schema.validator';

@Component({
  selector: 'app-purchase-invoice-add',
  templateUrl: './purchase-invoice-add.component.html',
  styleUrls: ['./purchase-invoice-add.component.scss'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'ja-JP'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS}
  ]
})
export class PurchaseInvoiceAddComponent implements OnInit {

  componentTitle: string;
  cardTitle: string;
  purchaseInvoiceForm: FormGroup;
  addressForm: FormGroup;
  counterpartyForm: FormGroup;
  errorMessage: string;
  taxRate = TaxRate;
  maxDate = new Date();
  displayedColumns: string[] = ['name', 'net', 'taxRate', 'vat', 'gross', 'editButton', 'deleteButton'];
  elements: Array<InvoiceElement> = new Array<InvoiceElement>();
  data: MatTableDataSource<InvoiceElement>;
  invoiceElementDialog: MatDialogRef<InvoiceElementComponent>;
  reckoningsNames: ReckoningItem[];
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;
  isLoadingDataFromGUS = false;

  amountPattern = /^[+-]?[0-9]+([.]([0-9]){1,2})?$/;
  // error messages
  invalidAmountMsg = 'Pole wymagane w postaci: 0.00';
  requiredMsg = 'Pole wymagane';
  invalidNIPMsg = 'Błędny NIP: wmagane 10 cyfr';
  invalidZipCodeMsg = 'Pole wymagane w postaci: NN-NNN';

  purchaseInvoice: PurchaseInvoice;
  editMode = false;
  readMode = false;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private purchaseInvoiceService: PurchaseInvoiceService,
              private reckoningService: ReckoningService,
              private gusService: GusService,
              private snackBar: MatSnackBar,
              private adapter: DateAdapter<any>,
              private dialog: MatDialog,
              private invoiceProvider: InvoiceProvider,
              private location: Location) {
  }

  ngOnInit() {
    console.log(`Inject invoice from provider: ${JSON.stringify(this.invoiceProvider.storage)}`);
    console.log(`current URL: ${this.router.url}`);
    if (this.router.url.includes('edit') && this.invoiceProvider.storage) {
      this.purchaseInvoice = <PurchaseInvoice>JSON.parse(this.invoiceProvider.storage);
      this.elements = Array.from(this.purchaseInvoice.invoiceElements);
      this.editMode = true;
      this.componentTitle = 'Faktura zakupu';
      this.cardTitle = 'Edycja faktury zakupu';
      console.log(`Purchase invoice in edit mode in providers with id: ${this.purchaseInvoice.invoiceId}`);

    } else if (this.router.url.includes('details') && this.invoiceProvider.storage) {
      this.purchaseInvoice = <PurchaseInvoice>JSON.parse(this.invoiceProvider.storage);
      this.elements = Array.from(this.purchaseInvoice.invoiceElements);
      this.readMode = true;
      this.componentTitle = 'Faktura zakupu';
      this.cardTitle = 'Dane faktury zakupu';
      console.log(`Purchase invoice in read mode in providers with id: ${this.purchaseInvoice.invoiceId}`);

    } else {
      this.componentTitle = 'Nowa faktura zakupu';
      this.cardTitle = 'Dodanie faktury zakupu';
    }

    this.adapter.setLocale('pl');

    this.reckoningService.getReckoningNames().subscribe(result => {
        console.log(`Get reckoning with suggested reckoning name: ${result.currentSuggestedReckoning.name}`);
        this.reckoningsNames = result.reckonings;
        this.form.reckoningName.setValue(result.currentSuggestedReckoning.name);
      },
      err => {
        console.log(`Error by getting reckonings: ${err}`);
        this.snackBar.open(`Wystąpił bład. Prosimy spróbować ponownie`, 'Zamknij', {duration: 10000});

      });

    this.addressForm = this.formBuilder.group({
      voivodeship: this.purchaseInvoice ? new FormControl({
        value: this.purchaseInvoice.counterparty.address.voivodeship,
        disabled: this.readMode
      }) : ['', Validators.required],
      county: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.counterparty.address.county, disabled: this.readMode})
        : ['', Validators.required],
      zipCode: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.counterparty.address.zipCode, disabled: this.readMode})
        : ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]{2}-[0-9]{3}$/)])],
      borough: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.counterparty.address.borough, disabled: this.readMode})
        : ['', Validators.required],
      town: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.counterparty.address.town, disabled: this.readMode})
        : ['', Validators.required],
      street: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.counterparty.address.street, disabled: this.readMode})
        : [''],
      buildingNumber: this.purchaseInvoice ? new FormControl({
        value: this.purchaseInvoice.counterparty.address.buildingNumber,
        disabled: this.readMode
      }) : ['', Validators.required],
      addressId: this.purchaseInvoice ? this.purchaseInvoice.counterparty.address.addressId : null
    });

    this.counterpartyForm = this.formBuilder.group({
      name: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.counterparty.name, disabled: this.readMode})
        : ['', Validators.required],
      nip: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.counterparty.nip, disabled: this.readMode})
        : ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]{10}$/)])],
      address: this.addressForm,
      counterpartyId: this.purchaseInvoice ? this.purchaseInvoice.counterparty.counterpartyId : null
    }, {
      validator: [ValidNIPSchema('nip')],
    });

    this.purchaseInvoiceForm = this.formBuilder.group({
      number: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.number, disabled: this.readMode})
        : ['', [Validators.required]],
      dateOfIssue: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.dateOfIssue, disabled: this.readMode})
        : ['', Validators.required],
      reckoningName: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.reckoningName, disabled: this.readMode})
        : ['', Validators.required],
      netHigh: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.netHigh, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      netMedium: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.netMedium, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      netLow: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.netLow, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      vatHigh: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.vatHigh, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      vatMedium: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.vatMedium, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      vatLow: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.vatLow, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      gross: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.gross, disabled: this.readMode})
        : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
      deducted: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.deducted, disabled: this.readMode})
        : new FormControl(true),
      fixedAsset: this.purchaseInvoice ? new FormControl({value: this.purchaseInvoice.fixedAsset, disabled: this.readMode})
        : new FormControl(false),
      counterparty: this.counterpartyForm,
      invoiceElements: this.elements,
      invoiceId: this.purchaseInvoice ? this.purchaseInvoice.invoiceId : null
    });

    this.data = new MatTableDataSource<InvoiceElement>(Array.from(this.elements));
  }

  compareFn(reckoning1: String, reckoning2: String): boolean {
    return reckoning1 === reckoning2;
  }

  get addressFormFields() {
    return this.addressForm.controls;
  }

  get counterpartyFields() {
    return this.counterpartyForm.controls;
  }

  get form() {
    return this.purchaseInvoiceForm.controls;
  }

  openDialog(element?): void {
    this.invoiceElementDialog = this.dialog.open(InvoiceElementComponent, {
      hasBackdrop: false,
      data: {
        name: element ? element.name : ['', [Validators.required]],
        net: element ? element.net : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
        taxRate: element ? element.taxRate : ['', [Validators.required]],
        vat: element ? element.vat : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
        gross: element ? element.gross : ['', [Validators.required, Validators.pattern(this.amountPattern)]],
        invoiceElementId: element ? element.invoiceElementId : null,
      },
      width: '450px'
    });

    this.invoiceElementDialog.afterClosed().subscribe(result => {
      console.log(`The dialog was closed with result name: ${result.name} for element with id ${result.invoiceElementId}`);
      if (result) {
        if (element) {
          const index: number = this.elements.indexOf(element);
          if (index !== -1) {
            this.elements[index] = result;
          }
        } else {
          this.elements.push(result);
        }
        console.log(`Result data: name: ${result['name']}, taxRate: ${result.taxRate}, id: ${result['invoiceElementId']}`);
        this.data = new MatTableDataSource<InvoiceElement>(this.elements);
        this.form.invoiceElements.setValue(this.elements);
        this.countSuggestedSummary();
      } else {
        console.log('Only undo');
      }
    });
  }

  deleteElement(element): void {
    console.log(`Deleting element with name ${element.name}`);
    const index: number = this.elements.indexOf(element);
    if (index !== -1) {
      this.elements.splice(index, 1);
      this.data = new MatTableDataSource<InvoiceElement>(this.elements);
      this.form.invoiceElements.setValue(this.elements);
      this.countSuggestedSummary();
    }
  }

  countSuggestedSummary() {
    let sumNetHigh = 0.0;
    let sumVatHigh = 0.0;
    let sumNetMedium = 0.0;
    let sumVatMedium = 0.0;
    let sumNetLow = 0.0;
    let sumVatLow = 0.0;
    let gross = 0.0;

    for (const element of this.elements) {
      if (parseInt(TaxRate[element.taxRate], 10) === parseInt(TaxRate.HIGH.toString(), 10)) {
        sumNetHigh += parseFloat(element.net.toString());
        sumVatHigh += element.vat;
      } else if (parseInt(TaxRate[element.taxRate], 10) === parseInt(TaxRate.MEDIUM.toString(), 10)) {
        sumNetMedium += parseFloat(element.net.toString());
        sumVatMedium += element.vat;
      } else {
        sumNetLow += parseFloat(element.net.toString());
        sumVatLow += element.vat;
      }
      gross += element.gross;
    }

    this.form.netHigh.setValue(Math.round(sumNetHigh * 100) / 100);
    this.form.vatHigh.setValue(Math.round(sumVatHigh * 100) / 100);
    this.form.netMedium.setValue(Math.round(sumNetMedium * 100) / 100);
    this.form.vatMedium.setValue(Math.round(sumVatMedium * 100) / 100);
    this.form.netLow.setValue(Math.round(sumNetLow * 100) / 100);
    this.form.vatLow.setValue(Math.round(sumVatLow * 100) / 100);
    this.form.gross.setValue(Math.round(gross * 100) / 100);
  }

  countSuggestedGross() {
    if (this.purchaseInvoiceForm.value.netHigh !== '' && this.purchaseInvoiceForm.value.vatHigh !== ''
      && this.purchaseInvoiceForm.value.netMedium !== '' && this.purchaseInvoiceForm.value.vatMedium !== ''
      && this.purchaseInvoiceForm.value.netLow !== '' && this.purchaseInvoiceForm.value.vatLow !== '') {
      const suggestedGross = parseFloat(this.purchaseInvoiceForm.value.netHigh) + parseFloat(this.purchaseInvoiceForm.value.vatHigh)
        + parseFloat(this.purchaseInvoiceForm.value.netMedium) + parseFloat(this.purchaseInvoiceForm.value.vatMedium)
        + parseFloat(this.purchaseInvoiceForm.value.netLow) + parseFloat(this.purchaseInvoiceForm.value.vatLow);
      this.form.gross.setValue(suggestedGross);
    }
  }

  onSubmit(form: NgForm) {
    console.log(`Submitting new purchase invoice form with number of elements = ${form['invoiceElements'].length}`);
    if (this.purchaseInvoiceForm.invalid) {
      console.log('Invalid data in new purchase invoice form');
      return;
    }
    const selectedReckoning = this.findSelectedReckoning(form['reckoningName']);
    if (selectedReckoning.auditFileId !== null) {
      this.submitAfterConfirmation(form);
    } else {
      this.submitInvoiceForm(form);
    }
  }

  private findSelectedReckoning(reckoningName: string): ReckoningItem {
    return this.reckoningsNames.find(reckoning => reckoning.name === reckoningName);
  }

  private submitInvoiceForm(form: NgForm) {
    if (this.editMode) {
      console.log('Send updated purchase invoice');
      form['invoiceElements'] = this.elements;
      console.log(`Submitting updated purchase invoice form with number of elements = ${form['invoiceElements'].length}`);
      this.purchaseInvoiceService.update(this.purchaseInvoice.invoiceId, form).subscribe(result => {
          const purchaseRecordId = result['recordId'];
          this.router.navigate([`/record/purchase/${purchaseRecordId}`]);
          this.errorMessage = '';
          this.snackBar.open(`Zaktualizowano dane zakupu faktury o numerze: ${result['number']}`, 'Zamknij', {duration: 10000});
        },
        error => {
          this.errorMessage = error;
        });
    } else {
      this.purchaseInvoiceService.create(form).subscribe(result => {
          const purchaseRecordId = result['recordId'];
          this.router.navigate([`/record/purchase/${purchaseRecordId}`]);
          this.errorMessage = '';
          this.snackBar.open(`Dodano fakturę zakupu o numerze: ${result['number']}`, 'Zamknij', {duration: 10000});
        },
        error => {
          this.errorMessage = error;
        });
    }
  }

  private submitAfterConfirmation(form: NgForm) {
    console.log(`Adding purchase invoice to reckoning with jpk`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Faktura zakupu dla istniejącego JPK',
        message: 'Faktura zostanie dodana do okresu, dla którego wygenerowano już JPK. Czy chcesz kontynuować?'
      }
    });

    this.confirmationDialog.afterClosed().subscribe(value => {
      if (value) {
        this.submitInvoiceForm(form);
      }
    });
    return;
  }

  onGUSDataButtonClick(nip: number) {
    this.isLoadingDataFromGUS = true;
    console.log('Downloading data from GUS for nip: ', nip);
    this.gusService.getDataFromGus(nip)
      .subscribe(
        result => {
          this.counterpartyFields.name.setValue(result.name);
          this.addressFormFields.voivodeship.setValue(result.province);
          this.addressFormFields.county.setValue(result.county);
          this.addressFormFields.zipCode.setValue(result.zipCode);
          this.addressFormFields.borough.setValue(result.borough);
          this.addressFormFields.town.setValue(result.town);
          this.addressFormFields.street.setValue(result.street);
          this.errorMessage = '';
          this.isLoadingDataFromGUS = false;
        },
        error => {
          console.log(error.toString());
          this.errorMessage = error;
          this.isLoadingDataFromGUS = false;
        });
  }

  undoClick() {
    this.location.back();
  }
}
