import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PurchaseInvoiceAddComponent} from '../purchase-invoice-add/purchase-invoice-add.component';
import {TaxpayerRoleGuard} from '../../../security/guards/taxpayer-role-guard';
import {PurchaseInvoiceListComponent} from '../purchase-invoice-list/purchase-invoice-list.component';

const routes: Routes = [
  {path: 'purchase-add', component: PurchaseInvoiceAddComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'purchase-edit/:addressId', component: PurchaseInvoiceAddComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'purchase-details/:addressId', component: PurchaseInvoiceAddComponent, canActivate: [TaxpayerRoleGuard]},
  {path: 'purchase-list', component: PurchaseInvoiceListComponent, canActivate: [TaxpayerRoleGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseInvoicesRoutingModule {
}
