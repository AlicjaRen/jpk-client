import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {PurchaseInvoice} from '../model/purchase-invoice';
import {MatDialog, MatDialogRef, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {TaxRate} from '../../shared/models/tax-rate.enum';
import {Router} from '@angular/router';
import {InvoiceProvider} from '../../shared/providers/invoice-provider';
import {PurchaseInvoiceService} from '../services/purchase-invoice.service';
import {ConfirmationDialogComponent} from '../../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';
import {SaleInvoice} from '../../sale-invoices/model/sale-invoice';

@Component({
  selector: 'app-purchase-invoice-list',
  templateUrl: './purchase-invoice-list.component.html',
  styleUrls: ['./purchase-invoice-list.component.scss']
})
export class PurchaseInvoiceListComponent implements OnInit, OnChanges {

  @Input() invoices: PurchaseInvoice[];

  data: MatTableDataSource<PurchaseInvoice>;
  displayedColumns: string[] = ['no', 'dateOfIssue', 'number', 'nip', 'name', 'netLow', 'vatLow', 'netMedium', 'vatMedium', 'netHigh',
    'vatHigh', 'gross', 'fixedAsset', 'deducted', 'editButton', 'deleteButton'];
  taxRate = TaxRate;
  confirmationDialog: MatDialogRef<ConfirmationDialogComponent>;

  constructor(private router: Router,
              private invoiceProvider: InvoiceProvider,
              private purchaseInvoiceService: PurchaseInvoiceService,
              private snackBar: MatSnackBar,
              private dialog: MatDialog) {
  }

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.data = new MatTableDataSource<PurchaseInvoice>(this.invoices);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  onRowClick(invoice: PurchaseInvoice) {
    console.log(`Details for purchase invoice with id: ${invoice.invoiceId} and number: ${invoice.number}`);
    this.invoiceProvider.storage = JSON.stringify(invoice);
    console.log(`In storage saved: ${JSON.stringify(this.invoiceProvider.storage)}`);
    this.router.navigate([`/invoice/purchase/purchase-details/${invoice.invoiceId}`]);
  }

  onEditClick(invoice: PurchaseInvoice) {
    console.log(`Edit purchase invoice with id: ${invoice.invoiceId}`);
    this.invoiceProvider.storage = JSON.stringify(invoice);
    console.log(`In storage saved: ${JSON.stringify(this.invoiceProvider.storage)}`);
    this.router.navigate([`/invoice/purchase/purchase-edit/${invoice.invoiceId}`]);
  }

  onDeleteClick(invoice: PurchaseInvoice) {
    console.log(`Delete purchase invoice with id: ${invoice.invoiceId}`);
    this.confirmationDialog = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      hasBackdrop: false,
      data: {
        title: 'Usuwanie faktury',
        message: `Faktura u numerze: ${invoice.number} zostanie usunięta. Czy chesz kontynuować?`
      }
    });

    this.confirmationDialog.afterClosed().subscribe((value) => {
      if (value) {
        this.deleteInvoice(invoice);
      }
    });
  }

  deleteInvoice(invoice: SaleInvoice) {
    this.purchaseInvoiceService.delete(invoice.invoiceId).subscribe((response) => {
        console.log(`Purchase invoice deleted, response: ${response}`);
        this.router.navigate([`/record/${invoice.recordId}`]);
        this.snackBar.open(`Usunięto zakupu fakturę o numerze: ${invoice.number}`, 'Zamknij', {duration: 10000});
      },
      error => {
        console.log(`Error by deleting invoice: ${error}`);
        this.snackBar.open(`Błąd podczas usuwania faktury zakupu o numerze: ${invoice.number}. Prosimy spróbwać ponownie.`,
          'Zamknij', {duration: 10000});
      });
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.data = new MatTableDataSource<PurchaseInvoice>(this.invoices);
    this.data.paginator = this.paginator;
    this.data.sort = this.sort;
  }

  getBooleanViewValue(booleanValue: boolean): string {
    if (booleanValue) {
      return 'TAK';
    } else {
      return 'NIE';
    }
  }
}
