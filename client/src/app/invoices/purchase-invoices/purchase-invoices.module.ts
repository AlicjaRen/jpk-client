import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PurchaseInvoiceAddComponent} from './purchase-invoice-add/purchase-invoice-add.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../angular-material/angular-material.module';
import {CommonTemplatesModule} from '../../common-templates/common-templates.module';
import {PurchaseInvoicesRoutingModule} from './routes/purchase-invoices-routing.module';
import {PurchaseInvoiceListComponent} from './purchase-invoice-list/purchase-invoice-list.component';
import {InvoiceElementComponent} from '../shared/invoice-element/invoice-element.component';
import {SharedModule} from '../shared/shared.module';
import {ConfirmationDialogComponent} from '../../common-templates/dialog/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    PurchaseInvoicesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    CommonTemplatesModule,
    SharedModule
  ],
  declarations: [
    PurchaseInvoiceAddComponent,
    PurchaseInvoiceListComponent
  ],
  exports: [
    PurchaseInvoiceListComponent
  ],
  entryComponents: [
    InvoiceElementComponent,
    ConfirmationDialogComponent
  ]
})
export class PurchaseInvoicesModule {
}
