import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../angular-material/angular-material.module';
import {CommonTemplatesModule} from '../common-templates/common-templates.module';
import {InvoicesRoutingModule} from './routes/invoices-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    InvoicesRoutingModule,
    FormsModule,
    AngularMaterialModule,
    CommonTemplatesModule
  ]
})
export class InvoicesModule {
}
